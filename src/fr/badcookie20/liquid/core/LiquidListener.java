package fr.badcookie20.liquid.core;

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.GUIExecutor;
import fr.badcookie20.liquid.core.editor.adder.*;
import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This listener is the main listener that listens every event for the main plugin
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        EditionPlayer.registerPlayer(e.getPlayer());
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        EditionPlayer.removePlayer(EditionPlayer.getEditionPlayer(e.getPlayer()));
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if(NextUtils.needsCancel(e.getPlayer())) {
            e.setCancelled(true);
            NextUtils.updateCancel(e.getPlayer());
        }

        NextUtils.updateChatOf(e.getPlayer(), e.getMessage());
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        NextUtils.updateClickedItemOf((Player) e.getPlayer(), NextUtils.CLOSED_INVENTORY);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getItem().equals(EditionPlayer.GUI_ITEM)) {
            GUIExecutor.openGui(e.getPlayer());
        }
    }

    @EventHandler
    public void onInventory(InventoryClickEvent e) throws LiquidEditionException {
    	NextUtils.updateClickedItemOf((Player) e.getWhoClicked(), e.getCurrentItem());
    	
    	if(!MicroPluginUtils.isEditionInventory(e.getInventory())) return;
    	
    	e.getWhoClicked().closeInventory();

    	ItemStack item = e.getCurrentItem();
        if(item == null || item.getType() == Material.AIR) return;
    	String itemName = item.getItemMeta().getDisplayName();
    	String invName = e.getInventory().getName();
    	MicroPlugin microplugin = MicroPluginManager.getInstance().getMicroPluginByName(new StringBuilder(invName).deleteCharAt(0).toString());

    	Player p = (Player) e.getWhoClicked();
        EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

        if(editionPlayer == null) throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
        
    	if(itemName.contains("About")) {
    		microplugin.sendInfos(p);
    	}else if(itemName.contains("Close")) {
        }else if(itemName.contains("edition")) {
            editionPlayer.turnOffEditing();
        }else if(itemName.contains("Erase")) {
            editionPlayer.emptyEdition();
        }else if(itemName.contains("Delete")) {
            MicroPluginUtils.deleteMicroPlugin(p, editionPlayer);
        }else if(itemName.contains("command")) {
            editionPlayer.setInAdvancedEdition();
            BukkitUtils.disableAsyncCatcher();
            new Thread() {
            	@Override
            	public void run() {
            		new CommandAdderSystem().interactWithPlayer(p, editionPlayer, editionPlayer.getEditing());
            	}
            }.start();
        }else if(itemName.contains("event")) {
            editionPlayer.setInAdvancedEdition();
            BukkitUtils.disableAsyncCatcher();
            new Thread() {
            	@Override
            	public void run() {
            		new EventAdderSystem().interactWithPlayer(p, editionPlayer, editionPlayer.getEditing());
            	}
            }.start();
        }else if(itemName.contains("var")) {
            editionPlayer.setInAdvancedEdition();
            BukkitUtils.disableAsyncCatcher();
            new Thread() {
                @Override
                public void run() {
                    new VarAdderSystem().interactWithPlayer(p, editionPlayer, editionPlayer.getEditing());
                }
            }.start();
        }else if(itemName.contains("property")) {
            editionPlayer.setInAdvancedEdition();
            BukkitUtils.disableAsyncCatcher();
            new Thread() {
                @Override
                public void run() {
                    new PropertyAdderSystem().interactWithPlayer(p, editionPlayer, editionPlayer.getEditing());
                }
            }.start();
        }else if(itemName.contains("structure")) {
            editionPlayer.setInAdvancedEdition();
            BukkitUtils.disableAsyncCatcher();
            new Thread() {
                @Override
                public void run() {
                    new StructAdderSystem().interactWithPlayer(p, editionPlayer, editionPlayer.getEditing());
                }
            }.start();
        }
    }

}
