package fr.badcookie20.liquid.core;

import fr.badcookie20.liquid.core.commands.CommandRegisterer;
import fr.badcookie20.liquid.core.editor.*;
import fr.badcookie20.liquid.core.editor.gui.*;
import fr.badcookie20.liquid.core.events.CustomReloadEvent;
import fr.badcookie20.liquid.core.exceptions.LiquidCommandException;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
/**
 * This class is the plugin main class.
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidPlugin extends JavaPlugin {

	public static final boolean SHOW_STACK_TRACE = false;
	public static final String VERSION = "Release 1.1";
    private static final boolean IS_INDEV = true;
	private static LiquidPlugin instance;
	
	@Override
	public void onEnable() {
		instance = this;

        Logger.logInfo("Loaded successfully the LiquidPlugin!");

        this.getCommand("edit").setExecutor(new EditExecutor());
        this.getCommand("add").setExecutor(new AddExecutor());
        this.getCommand("about").setExecutor(new AboutExecutor());
        this.getCommand("create").setExecutor(new CreateExecutor());
        this.getCommand("delete").setExecutor(new DeleteExecutor());
        this.getCommand("gui").setExecutor(new GUIExecutor());
        this.getCommand("cancel").setExecutor(new CancelExecutor());

        EditionPlayer.registerOnlinePlayers();
        BukkitUtils.registerListeners(new LiquidListener());

        try {
            CommandRegisterer.init();
        } catch (LiquidCommandException e) {
            e.printStackTrace();
        }

        MicroPluginManager.init();
        new HomeInventory();
        new ActionsInventory();
        new EventsInventory();
        new VarTypeInventory();
        new PropertiesInventory();
        new RequirementsInventory();

        Logger.logInfo("LiquidPlugin was initialized! The stack traces are " + (SHOW_STACK_TRACE ? "shown" : "not shown"));
        if(IS_INDEV) {
            Logger.logWarning("This LiquidPlugin version is in development. It may contain bugs, be careful!");
        }

        Bukkit.getServer().getPluginManager().callEvent(new CustomReloadEvent());
    }

    @Override
    public void onDisable() {
        EditionPlayer.removeAllPlayers();
        BukkitUtils.enableAsyncCatcher();
    }
	
	public static LiquidPlugin getInstance() {
		return instance;
	}
	
}
