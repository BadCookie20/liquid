package fr.badcookie20.liquid.core.actions;

import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidUnsupportedStatementException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.particles.ParticleEffect;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.utils.LangUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.Math.*;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
/**
 * This class allows some Bukkit and Spigot methods to be accessed in a static way (with the instance in parameter) if the method is too difficult
 * to be executed by the default system
 * @author BadCookie20
 * @since InDev 3.0
 * @see LiquidAction
 */
public class ActionEmulator {

    private static List<ParticleEffect> authorizedParticles;
    private static List<Sound> authorizedSounds;

    static {
        authorizedParticles = new ArrayList<>();
        authorizedParticles.addAll(Arrays.asList(
                ParticleEffect.FIREWORKS_SPARK,
                ParticleEffect.EXPLOSION_NORMAL,
                ParticleEffect.SPELL_WITCH,
                ParticleEffect.SPELL_MOB,
                ParticleEffect.SPELL_MOB_AMBIENT,
                ParticleEffect.SMOKE_LARGE,
                ParticleEffect.SMOKE_NORMAL,
                ParticleEffect.FLAME,
                ParticleEffect.SPELL_INSTANT));

        authorizedSounds = new ArrayList<>();
        authorizedSounds.addAll(Arrays.asList(
                Sound.EXPLODE,
                Sound.FIREWORK_LARGE_BLAST2,
                Sound.FIREWORK_LARGE_BLAST,
                Sound.FIREWORK_BLAST,
                Sound.FIREWORK_LARGE_BLAST2,
                Sound.CREEPER_HISS));
    }

    public static void setVar(String varName, Object newValue) throws LiquidException {
        for(MicroPlugin microPlugin : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            for(LiquidParameter var : microPlugin.getAllVars()) {
                String path = var.getLiquidObject().getPath();
                String[] parts = LangUtils.split(path, '_');
                if(parts[2].equals(varName)) {
                    if(parts[0].equals("const")) {
                        throw new LiquidUnsupportedStatementException("the value of the constant " + varName + " cannot be modified!");
                    }

                    var.setContent(newValue);

                    System.out.println("Modified constant " + varName + ". New value : " + newValue);

                    microPlugin.updateVar(var);
                }
            }
        }
    }

    /**
     * Sets the time of the given world
     * @param time the time in milliseconds
     * @param w the world
     */
    public static void setTime(Number time, World w) {
        w.setTime(time.longValue());
    }

    /**
     * Gives the specified player the specified ItemStack
     * @param p the player
     * @param item the item
     */
    public static void addItem(Player p, ItemStack item) {
        p.getInventory().addItem(item);
    }

    /**
     * Increments the level of the specified player
     * @param p the player
     * @param level the level increment
     */
    public static void incrementLevel(Player p, Number level) {
        p.setLevel(p.getLevel() + level.intValue());
    }

    /**
     * Sets the specified player the specified level(s)
     * @param p the player
     * @param level the level(s)
     */
    public static void setLevel(Player p, Number level) {
        p.setLevel(level.intValue());
    }

    /**
     * Decrements the level of the specified player
     * @param p the player
     * @param level the level decrement
     */
    public static void decrementLevel(Player p, Number level) {
        p.setLevel(p.getLevel() - level.intValue());
    }

    /**
     * Gives the specified player an ItemStack with the specified material
     * @param p the player
     * @param mat the material of the ItemStack
     */
    public static void addItem(Player p, Material mat) {
    	p.getInventory().addItem(new ItemStack(mat));
    }

    /**
     * Teleports the specified player to the specified coordinates
     * @param p the player to teleport
     * @param x the x-axis coordinate
     * @param y the y-axis coordinate
     * @param z the x-axis coordinate
     */
    public static void teleport(Player p, Number x, Number y, Number z) {
        Location l = new Location(p.getWorld(), x.intValue(), y.intValue(), z.intValue());
        p.teleport(l);
    }

    /**
     * Teleports the specified player to the specified location
     * @param p the player to teleport
     * @param l the location
     */
    public static void teleport(Player p, Location l) {
        p.teleport(l);
    }

    /**
     * Places a block with the specified Material at the specified location
     * @param l the location
     * @param type the type of the block
     */
    public static void placeBlock(Location l, Material type) {
        Block b = l.getWorld().getBlockAt(l);
        b.setType(type);
    }

    /**
     * Displays a sphere of the specified radius around the specified location
     * @param l the location
     * @param rad the radius of the sphere
     */
    public static void displayParticlesSphere(Location l, Number rad) {
        final ParticleEffect pE = getRandomParticleEffect();

        int radius = rad.intValue();

        for(double phi = 0; phi <= PI; phi+=PI/30) {
            for(double theta = 0; theta <= 2*PI; theta += PI/40) {
                double x = radius*cos(theta)*sin(phi);
                double y = radius*cos(phi) + 1.5;
                double z = radius*sin(theta)*sin(phi);
                l.add(x, y, z);
                pE.display(0, 0, 0, (float) 0.1, 1, l, 50);
                l.subtract(x, y, z);
            }
        }
    }

    /**
     * Displays a sphere of the specified radius around the specified player's head
     * @param p the player
     * @param rad the radius of the sphere
     */
    public static void displayParticlesAroundHead(Player p, Number rad) {
    	final ParticleEffect pE = getRandomParticleEffect();
    	final Location l = p.getEyeLocation();
    	
    	int radius = rad.intValue();
    	
    	for(double phi = 0; phi <= PI; phi+=PI/30) {
    		for(double theta = 0; theta <= 2*PI; theta +=PI/40) {
    			double x = radius*cos(theta)*sin(phi);
    			double z = radius*sin(theta)*sin(phi);
    			l.add(x, 0, z);
    			pE.display(0, 0, 0, (float) 0.1, 1, l, 50);
                l.subtract(x, 0, z);
    		}
    	}
    }

    /**
     * Returns a random sound of the {@link #authorizedSounds} list
     * @return a random sound
     */
    private static Sound getRandomSound() {
        Random r = new Random();
        int random = r.nextInt(authorizedSounds.size());

        return authorizedSounds.get(random);
    }

    /**
     * Returns a random ParticleEffect of the {@link #authorizedParticles} list
     * @return a random ParticleEffect
     */
    private static ParticleEffect getRandomParticleEffect() {
        Random r = new Random();
        int random = r.nextInt(authorizedParticles.size());

        return authorizedParticles.get(random);
    }

}
