package fr.badcookie20.liquid.core.actions;

import fr.badcookie20.liquid.core.condition.ConditionInstance;
import fr.badcookie20.liquid.core.exceptions.LiquidActionException;
import fr.badcookie20.liquid.core.exceptions.LiquidConditionFormatException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.reflect.LiquidObject;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.ParameterUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the action instance which is used for the execution of an action
 * @author BadCookie20
 * @since InDev 2.0
 * @see LiquidAction
 */
public class ActionInstance {

    private List<LiquidParameter> inputArgs;
    private LiquidParameter inputInstance;
    private LiquidAction relatedAction;
    private ConditionInstance conditionInstance;

    /**
     * Constructor of the instance
     * @param inputInstance the instance parameter (should be null if the related action is static)
     * @param inputArgs the arguments of the action (the list should be empty is there are no parameters needed)
     * @param relatedAction the related LiquidAction
     */
    public ActionInstance(LiquidParameter inputInstance, List<LiquidParameter> inputArgs, LiquidAction relatedAction) {
        this.inputArgs = inputArgs;
        this.inputInstance = inputInstance;
        this.relatedAction = relatedAction;
    }

    /**
     * Replaces the instance to the specified parameter, which has to be of the same type of the previous instance parameter
     * @param instance the instance to apply
     * @throws LiquidException if the type needed for the instance cannot be retrieved, or if the instance specified is not the type expected
     */
    public void replaceInstance(LiquidParameter instance) throws LiquidException {
        LiquidObject paramLiquidObject = instance.getLiquidObject();
        LiquidRequirement paramRequirement = paramLiquidObject.getRequirement();
        if(paramRequirement == null) {
            throw new LiquidActionException("unknown parameter " + paramLiquidObject.getPath());
        }

        Class<?> paramClassType = paramRequirement.getClassType();
        if(!relatedAction.getInstanceRequirement().getClassType().equals(paramClassType)) {
            throw new LiquidActionException("the instance of the action " + this.relatedAction.getMethod().getName() + " is not the type expected!");
        }

        for(MicroPlugin microPlugin : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            for(StructType struct : microPlugin.getAllStructs()) {
                if(struct.getName().equalsIgnoreCase(paramLiquidObject.getPath())) {
                    instance = ParameterUtils.fromStringToParam(paramLiquidObject.getPath());
                }
            }
        }

        this.inputInstance = instance;
    }

    /**
     * Returns the instance of this action instance
     * @return the instance parameter
     */
    public LiquidParameter getInstance() {
        return this.inputInstance;
    }

    /**
     * Replaces the previous condition with the specified one
     * @param cond the condition to apply to this action instance
     */
    public void replaceCondition(ConditionInstance cond) {
    	this.conditionInstance = cond;
    }

    /**
     * Returns the condition of this action instance
     * @return the condition parameter
     */
    public ConditionInstance getCondition() {
    	return this.conditionInstance;
    }

    /**
     * Replaces the previous arguments with the specified ones
     * @param inputArgs the new arguments
     */
    public void replaceArguments(List<LiquidParameter> inputArgs) {
        this.inputArgs = inputArgs;
    }

    /**
     * Returns the arguments of this action instance
     * @return the arguments parameters
     */
    public List<LiquidParameter> getArguments() {
        return this.inputArgs;
    }

    /**
     * Returns the related action of this instance
     * @return the action
     */
    public LiquidAction getRelatedAction() {
        return this.relatedAction;
    }

    /**
     * Empties all the characteristics (instace, arguments and condition) of this action instance
     */
    public void empty() {
        this.inputArgs = null;
        this.inputInstance = null;
        this.conditionInstance = null;
    }

    /**
     * Executes the related action with the instance arguments
     * @throws IllegalAccessException if the method of the action is not accessible
     * @throws IllegalArgumentException if the invoked arguments are not suitable to the method's arguments
     * @throws InvocationTargetException if the invoked method throws an exception
     * @throws LiquidActionException if the arguments are null or if the instance is null and the action needs one
     * @throws LiquidParameterException if an exception happens while interacting with parameters
     * @throws LiquidConditionFormatException if the condition cannot be checked
     */
    public void execute() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, LiquidActionException, LiquidParameterException, LiquidConditionFormatException {
        if(inputArgs == null || (inputInstance == null && !this.relatedAction.isStatic())) {
            throw new LiquidActionException("cannot execute the action " + this.relatedAction.toString() + " while the arguments or the instance is null!");
        }

        if(this.conditionInstance != null) {
        	if(!this.conditionInstance.isTrue()) {
        		return;
        	}
        }
        
        List<Object> argsToInvoke = new ArrayList<>();

        for(LiquidParameter lp : this.inputArgs) {
            argsToInvoke.add(lp.getContent());
            System.out.println("param: requirement: " + lp.getLiquidObject().getRequirement() + ", path: " + lp.getLiquidObject().getPath() + ", content:" + lp.getContent());
        }

        if(this.relatedAction.isStatic()) {
            this.relatedAction.getMethod().invoke(null, argsToInvoke.toArray());
        }else {
            this.relatedAction.getMethod().invoke(this.inputInstance.getContent(), argsToInvoke.toArray());

            if(!inputInstance.isUnereasable()) {
                this.inputInstance.setContent(null);
            }
        }

        for(LiquidParameter p : this.inputArgs) {
            if(!p.isUnereasable() && !ParameterUtils.isVar(p.getLiquidObject().getPath()) && !ParameterUtils.isConstant(p.getLiquidObject().getPath())) {
                p.setContent(null);
            }
        }
    }

}