package fr.badcookie20.liquid.core.actions;

import fr.badcookie20.liquid.core.exceptions.LiquidActionException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This enum is the enum which stores all the possible actions of the micro plugin. The action instances must be registered in {@link ActionInstance} and
 * the actions which need a facade to be accessed must be declared in {@link ActionEmulator}
 * @author BadCookie20
 * @since InDev 1.0
 * @see ActionEmulator
 * @see ActionInstance
 */
public enum LiquidAction {

    BROADCAST("broadcast",
            "Broadcasts a message to the server",
            Bukkit.class,
            "broadcastMessage",
            null,
            LiquidRequirement.REQUIRES_STRING),
    CHANGE_GAMEMODE("setgamemode",
            "Changes the gamemode of the specified player",
            Player.class,
            "setGameMode",
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_GAMEMODE),
    DECREMENT_LEVEL("lvl--",
            "Decrements the level of the specified player",
            ActionEmulator.class,
            "decrementLevel",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_NUMBER),
    DISPLAY_PARTICLES_AROUND_HEAD("parthead",
            "Displays particles around the head\nof the player",
            ActionEmulator.class,
            "displayParticlesAroundHead",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_NUMBER),
    DISPLAY_PARTICLES_SPHERE("partsphere",
            "Displays particles around the location",
            ActionEmulator.class,
            "displayParticlesSphere",
            null,
            LiquidRequirement.REQUIRES_LOCATION,
            LiquidRequirement.REQUIRES_NUMBER),
    GET_PROPERTY("get",
            "Gets the specified property of the\nspecified structure",
            StructType.class,
            "getProperty",
            LiquidRequirement.REQUIRES_STRUCT,
            LiquidRequirement.REQUIRES_STRING),
    GIVE_ITEM_BLOCK("give",
            "Gives the specified item to the player",
            ActionEmulator.class,
            "addItem", null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_ITEMSTACK),
    GIVE_ITEM_MATERIAL("give",
            "Gives an item of the specified type\nto the player",
            ActionEmulator.class,
            "addItem",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_MATERIAL),
    INCREMENT_LEVEL("lvl++",
            "Increments the level of the specified player",
            ActionEmulator.class,
            "incrementLevel",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_NUMBER),
    MODIFY_PROPERTY("modif",
            "Modifies the specified property of the\nspecified structure",
            StructType.class,
            "setProperty",
            LiquidRequirement.REQUIRES_STRUCT,
            LiquidRequirement.REQUIRES_STRING,
            LiquidRequirement.REQUIRES_OBJECT),
    MODIFY_VAR("modif",
            "Sets the value of the variable name entered\nto the one specified",
            ActionEmulator.class,
            "setVar",
            null,
            LiquidRequirement.REQUIRES_STRING,
            LiquidRequirement.REQUIRES_OBJECT),
    PLACE_BLOCK("place",
            "Places a block at the specified location",
            ActionEmulator.class,
            "placeBlock",
            null,
            LiquidRequirement.REQUIRES_LOCATION,
            LiquidRequirement.REQUIRES_MATERIAL),
    SEND_MESSAGE("sendMessage",
            "Sends a message to the specified player",
            Player.class,
            "sendMessage",
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_STRING),
    SET_LEVEL("lvl",
            "Sets the level of the player to the\nspecified number",
            ActionEmulator.class,
            "setLevel",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_NUMBER),
    SET_TIME("time",
            "Sets the time of the specified world\nto the specified time",
            ActionEmulator.class,
            "setTime",
            null,
            LiquidRequirement.REQUIRES_NUMBER,
            LiquidRequirement.REQUIRES_WORLD),
    STRIKE_LIGHTNING("strikelightning",
            "Strikes a lightning at the specified location",
            World.class,
            "strikeLightning",
            LiquidRequirement.REQUIRES_WORLD,
            LiquidRequirement.REQUIRES_LOCATION),
    TELEPORT("teleport",
            "Teleports the specified player to the\nspecified location",
            ActionEmulator.class,
            "teleport",
            null,
            LiquidRequirement.REQUIRES_PLAYER,
            LiquidRequirement.REQUIRES_LOCATION);

	private String actionName;
    private String description;
	private LiquidRequirement instanceRequirement;
    private Method m;
    private List<ActionInstance> actionsMaps;
    private List<LiquidRequirement> requirements;
    private boolean isStatic;

	LiquidAction(String actionName, String description, Class<?> executionClass, String methodName, LiquidRequirement instance, LiquidRequirement... args) {
        this.actionName = actionName;
        this.description = description;
		Class<?>[] argsObject = new Class<?>[args.length];
		int i = 0;
		for(LiquidRequirement r : args) {
			argsObject[i] = r.getClassType();
			i++;
		}

        this.requirements = Arrays.asList(args);

        try {
            try {
                this.m = executionClass.getMethod(methodName, argsObject);
            } catch (NoSuchMethodException | SecurityException e) {
                throw new LiquidActionException("error while initializing the action " + this.name() + "! The method is not accessible!");
            }
        }catch(LiquidActionException e) {
            e.printStackTrace();
        }

        if(instance == null) {
            this.isStatic = true;
        }

		this.instanceRequirement = instance;
        this.actionsMaps = new ArrayList<>();
	}
	
	public String getActionName() {
		return this.actionName;
	}

    public String getDescription() {
        return description;
    }
	
	public List<LiquidRequirement> getRequirements() {
		return this.requirements;
	}

    public ActionInstance newActionMap() {
        ActionInstance map = new ActionInstance(null, null, this);
        this.actionsMaps.add(map);
        return map;
    }

    public boolean isStatic() {
        return this.isStatic;
    }

    public LiquidRequirement getInstanceRequirement() {
        return instanceRequirement;
    }

    public Method getMethod() {
        return this.m;
    }

    public static LiquidAction findActionByName(String actionName, List<LiquidRequirement> requirements, boolean needsInstance) {
        for(LiquidAction a : values()) {
            if(!a.actionName.equalsIgnoreCase(actionName)) continue;
            if(MicroPluginUtils.areRequirementsEqual(a.requirements, requirements) && (needsInstance == !a.isStatic()) ) return a;
        }

        return null;
    }

    @Deprecated
	public static LiquidAction findActionByName(String actionName) {
        for(LiquidAction a : values()) {
            if(a.actionName.equalsIgnoreCase(actionName)) {
				return a;
			}
		}
		
		return null;
	}

    @Override
    public String toString() {
        return this.actionName;
    }
}
