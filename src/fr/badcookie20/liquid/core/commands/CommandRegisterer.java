package fr.badcookie20.liquid.core.commands;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.exceptions.LiquidCommandException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class that registers all the micro plugins command
 * @author BadCookie20
 * @since InDev 1.1
 * @see LiquidCommand
 */
public class CommandRegisterer {

	private static Field mapField;
	private static boolean init = false;
    private static List<Command> registeredCommands;

    /**
     * Inits the command registerer
     * @throws LiquidCommandException if the Bukkit system cannot be accessed
     */
	public static void init() throws LiquidCommandException {
		try {
			if(init) {
				Logger.logWarning("The command registerer is already initialized!");
				init = false;
			}
			Logger.logInfo("Trying to init the command registerer...");
			if (Bukkit.getServer() instanceof CraftServer) {
				Field f = CraftServer.class.getDeclaredField("commandMap");
				f.setAccessible(true);
				mapField = f;
			}
		} catch (Exception ex) {
			throw new LiquidCommandException("an error occured while trying to init the command registerer");
		}
		
		init = true;
		
		Logger.logInfo("Successfully initialized the command registerer!");

        registeredCommands = new ArrayList<>();
	}

    /**
     * Registers a new command with the specified parameters
     * @param cmdName the name of the command
     * @param actions the actions of the command
     * @param microPlugin the micro-plugin related with the action
     * @return the created LiquidCommand
     * @throws LiquidCommandException if the the {@link #init} method has not been executed,
     * if a command with the same name already exists,
     * or if the Bukkit system cannot be used
     */
    public static LiquidCommand registerLiquidCommand(String cmdName, List<ActionInstance> actions, MicroPlugin microPlugin) throws LiquidCommandException {
        if(!init) {
            throw new LiquidCommandException("cannot register commands while the command registerer is not initialized!");
        }
        try {
            LiquidCommand cmd = new LiquidCommand(cmdName, actions, microPlugin);
            if(registeredCommands.contains(cmd)) throw new LiquidCommandException("a command with name " + cmdName + " already exists!");
            CommandMap cmdMap = (CommandMap) mapField.get(Bukkit.getServer());
            if(cmdMap.getCommand(cmdName) != null) throw new LiquidCommandException("a command with name " + cmdName + " already exists!");
            cmdMap.register("", cmd);
            mapField.set(Bukkit.getServer(), cmdMap);
            registeredCommands.add(cmd);
            return cmd;
        }catch(IllegalAccessException e) {
            throw new LiquidCommandException("an error occured while accessing the command map. Please check your server version");
        }
    }

    /**
     * Removes the registered commands by this system from the Bukkit command system
     * @throws LiquidCommandException if the Bukkit command system is not accessible
     */
	public static void removeRegisteredCommands() throws LiquidCommandException {
        try {
            CommandMap cmdMap = (CommandMap) mapField.get(Bukkit.getServer());

            Field f = cmdMap.getClass().getDeclaredField("knownCommands");
            f.setAccessible(true);
            Map<String, Command> knownCommands = (Map<String, Command>) f.get(cmdMap);
            for(Command cmdKnown : knownCommands.values()) {
                if(registeredCommands.contains(cmdKnown)) {
                    registeredCommands.remove(cmdKnown);
                    knownCommands.remove(cmdKnown);
                }
            }
            f.set(cmdMap, knownCommands);
        }catch(IllegalAccessException | NoSuchFieldException ex) {
            ex.printStackTrace();
            throw new LiquidCommandException("cannot unregister commands from the micro-plugins! The field is not accessible");
        }
    }
}
