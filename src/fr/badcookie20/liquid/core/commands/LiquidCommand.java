package fr.badcookie20.liquid.core.commands;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidUnsupportedStatementException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.ParameterUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class where every custom command must pass by. This type of command should be registered with {@link CommandRegisterer}
 * @author BadCookie20
 * @since InDev 1.1
 * @see CommandRegisterer
 */
public class LiquidCommand extends Command {
	
	private static List<LiquidCommand> commands;
	
	static{
		commands = new ArrayList<>();
	}

	private String cmdName;
	private List<ActionInstance> actions;
	private MicroPlugin relatedMicroPlugin;

	public LiquidCommand(String cmdName, List<ActionInstance> actions, MicroPlugin relatedMicroPlugin) {
		super(cmdName);
		this.cmdName = cmdName;
		this.actions = actions;
        this.relatedMicroPlugin = relatedMicroPlugin;
    }
	
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] arg2) {
        try {
            if (!(sender instanceof Player)) {
                throw new LiquidUnsupportedStatementException("the custom command using a console sender is not supported yet!");
            }

            Player p = (Player) sender;
            final List<Object> possibleArguments = Arrays.asList(p);

            for(ActionInstance a : this.actions) {
                ParameterUtils.fillInAction(a, possibleArguments);
                a.execute();
            }
        }catch(LiquidException | IllegalAccessException | InvocationTargetException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }

		return false;
	}

	public List<ActionInstance> getActions() {
        return this.actions;
    }

	public MicroPlugin getRelatedMicroPlugin() {
    	return this.relatedMicroPlugin;
    }

	public String getCommandName() {
		return this.cmdName;
	}

    public static List<LiquidCommand> getCommands() {
    	return commands;
    }
	
	
}
