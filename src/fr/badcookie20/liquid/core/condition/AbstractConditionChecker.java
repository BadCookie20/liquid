package fr.badcookie20.liquid.core.condition;

import fr.badcookie20.liquid.core.exceptions.LiquidConditionFormatException;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This abstract class is the base for all the conditions which can be computed by the plugin
 * @author BadCookie20
 * @since InDev 2.0
 */
public abstract class AbstractConditionChecker {

	protected boolean areNumber(Object o1, Object o2) {
		return isNumber(o1) && isNumber(o2);
	}
	
	protected boolean isNumber(Object o) {
		return o instanceof Number;
	}
	
	protected Number toNumber(Object o) throws LiquidConditionFormatException {
		if(!isNumber(o)) {
			throw new LiquidConditionFormatException("cannot convert " + o.toString() + " to a number!");
		}
		
		return (Number) o;
	}
	
	public abstract boolean isConditionTrue(Object o1, Object o2) throws LiquidConditionFormatException;
	
}
