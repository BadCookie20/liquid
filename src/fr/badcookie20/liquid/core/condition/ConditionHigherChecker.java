package fr.badcookie20.liquid.core.condition;

import fr.badcookie20.liquid.core.exceptions.LiquidConditionFormatException;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This condition type checks whether the objects computed are higher or not
 * @author BadCookie20
 * @since InDev 2.0
 */
public class ConditionHigherChecker extends AbstractConditionChecker {

	@Override
	public boolean isConditionTrue(Object o1, Object o2) throws LiquidConditionFormatException {
		if(!areNumber(o1, o2)) {
			throw new LiquidConditionFormatException("the computed parameters " + o1.toString() + " and " + o2.toString() + " are not compatible with the >> condition!");
		}
		
		Number n1 = toNumber(o1);
		Number n2 = toNumber(o2);
		
		return n1.doubleValue() > n2.doubleValue();
	}

}
