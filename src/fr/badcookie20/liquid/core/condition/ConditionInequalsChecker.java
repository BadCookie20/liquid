package fr.badcookie20.liquid.core.condition;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This condition type checks whether the objects computed are inequal or not
 * @author BadCookie20
 * @since InDev 2.0
 */
public class ConditionInequalsChecker extends AbstractConditionChecker {

	@Override
	public boolean isConditionTrue(Object o1, Object o2) {
		return !o1.equals(o2);
	}
	
	
}
