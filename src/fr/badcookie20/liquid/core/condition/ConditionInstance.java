package fr.badcookie20.liquid.core.condition;

import fr.badcookie20.liquid.core.exceptions.LiquidConditionFormatException;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is used to check a condition within an action execution
 * @author BadCookie20
 * @since InDev 2.0
 */
public class ConditionInstance {

	private ConditionType type;
	private LiquidParameter param1;
	private LiquidParameter param2;
	
	public ConditionInstance(ConditionType type, LiquidParameter param1, LiquidParameter param2) {
		this.type = type;
		this.param1 = param1;
		this.param2 = param2;
	}
	
	public boolean isTrue() throws LiquidConditionFormatException {
		return this.type.getChecker().isConditionTrue(param1.getContent(), param2.getContent());
	}
	
	public LiquidParameter getParam1() {
		return this.param1;
	}
	
	public LiquidParameter getParam2() {
		return this.param2;
	}
	
	public void setParam1(LiquidParameter param1) {
		this.param1 = param1;
	}
	
	public void setParam2(LiquidParameter param2) {
		this.param2 = param2;
	}
	
}
