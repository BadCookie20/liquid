package fr.badcookie20.liquid.core.condition;

import fr.badcookie20.liquid.core.exceptions.LiquidConditionFormatException;
import fr.badcookie20.liquid.core.exceptions.LiquidConditionStatementException;

import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This enum stores all the conditions computable by the plugins
 * @author BadCookie20
 * @since InDev 2.0
 */
public enum ConditionType {

	EQUALS("==", new ConditionEqualsChecker()),
	HIGHER_OR_EQUAL(">=", new ConditionHigherOrEqualChecker()),
	HIGHER(">>", new ConditionHigherChecker()),
	LESS_OR_EQUAL("<=", new ConditionLessOrEqualChecker()),
	LESS("<<", new ConditionLessChecker());
	
	private String indicator;
	private AbstractConditionChecker checker;
	
	ConditionType(String indicator, AbstractConditionChecker checker) {
		this.indicator = indicator;
		this.checker = checker;
	}
	
	public AbstractConditionChecker getChecker() {
		return this.checker;
	}
	
	public static AbstractConditionChecker getCheckerByString(String indicator) throws LiquidConditionStatementException {
		for(ConditionType type : values()) {
			if(type.indicator.equalsIgnoreCase(indicator)) {
				return type.checker;
			}
		}
		
		throw new LiquidConditionStatementException("the computed condition checker " + indicator + " is not recognized!");
	}

	public static ConditionType getTypeByString(String indicator) throws LiquidConditionStatementException {
		for(ConditionType type : values()) {
			if(type.indicator.equalsIgnoreCase(indicator)) {
				return type;
			}
		}
		
		throw new LiquidConditionStatementException("the computed condition checker " + indicator + " is not recognized!");
	}
 	
	public static boolean check(String indicator, Object o1, Object o2) throws LiquidConditionStatementException, LiquidConditionFormatException {
		for(ConditionType type : values()) {
			if(!type.indicator.equalsIgnoreCase(indicator)) {
				continue;
			}
			
			return type.checker.isConditionTrue(o1, o2);
		}
		
		throw new LiquidConditionStatementException("the computed condition checker " + indicator + " is not recognized!");
	}
	
	public static List<String> getAllIndicators() {
		List<String> indicators = new ArrayList<>();
		for(ConditionType type : values()) {
			indicators.add(type.indicator);
		}
		
		return indicators;
	}
	
}
