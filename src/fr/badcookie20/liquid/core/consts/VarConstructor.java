package fr.badcookie20.liquid.core.consts;

import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.ParameterUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class contains all the methods to buildConstant constants
 * @author BadCookie20
 * @since InDev 3.0
 */
public class VarConstructor {

	private static LiquidParameter buildConstantParam(String constName, VarType type) {
		return new LiquidParameter("const_" + type.displayName() + "_" + constName);
	}

    private static LiquidParameter buildVarParam(String constName, VarType type) {
        return new LiquidParameter("var_" + type.displayName() + "_" + constName);
    }
	
	public static LiquidParameter buildLoc(LiquidParameter param, Number x, Number y, Number z) throws LiquidParameterException {
		param.setContent(new Location(Bukkit.getWorld("world"), x.intValue(), y.intValue(), z.intValue()));
		return param;
	}
	
	public static LiquidParameter buildLoc(LiquidParameter param, String w, Number x, Number y, Number z) throws LiquidParameterException {
		param.setContent(new Location(Bukkit.getWorld(w), x.intValue(), y.intValue(), z.intValue()));
		return param;
	}
	
	public static LiquidParameter buildItem(LiquidParameter param, Material mat) throws LiquidParameterException {
		param.setContent(new ItemStack(mat));
        return param;
	}
	
	public static LiquidParameter buildItem(LiquidParameter param, Material mat, Number amount) throws LiquidParameterException {
		param.setContent(new ItemStack(mat, amount.intValue()));
        return param;
	}
	
	public static LiquidParameter buildItem(LiquidParameter param, Material mat, String name) throws LiquidParameterException {
		ItemStack item = new ItemStack(mat);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(name);
		item.setItemMeta(itemMeta);
		param.setContent(item);
		return param;
	}

	public static LiquidParameter buildItem(LiquidParameter param, Material mat, Number amount, String name) throws LiquidParameterException {
		ItemStack item = new ItemStack(mat, amount.intValue());
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(name);
		item.setItemMeta(itemMeta);
		param.setContent(item);
		return param;
	}
	
	public static LiquidParameter buildBoolean(LiquidParameter param, Boolean b) throws LiquidParameterException {
		param.setContent(b);
		return param;
	}

	public static LiquidParameter buildWorld(LiquidParameter param, String name) throws LiquidParameterException {
        param.setContent(Bukkit.getWorld(name));
        return param;
    }

	public static LiquidParameter build(List<String> objects, String constName, VarType type, boolean constant) throws LiquidException {
		List<LiquidParameter> params = toVarParameters(objects, constant);
		List<Object> contents = prepareContents(params);

		if(!constant) {
            return buildVarParam(constName, type);
        }
		
		try {
			Method matched = match(params, type);
			if(matched == null) throw new LiquidParameterException("error while attempting to build the " + constName + " var! Its constructor does not match any!");
            Object result = invoke(matched, type, constName, contents);
			if(!(result instanceof LiquidParameter)) throw new LiquidParameterException("error while attempting ot buildConstant the " + constName + " constant! The internal method is corrupted!");
			return (LiquidParameter) result;
		}catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			throw new LiquidParameterException("error while attempting to buildConstant item constant " + constName + "! The constructor is not accessible!");
		}
	}

    private static Object invoke(Method m, VarType type, String constName, List<Object> objects) throws InvocationTargetException, IllegalAccessException {
        Object[] params = objects.toArray();

        LiquidParameter param = buildConstantParam(constName, type);

        if(m.getParameterCount() == 2) {
           return m.invoke(null, param, params[0]);
        }else if(m.getParameterCount() == 3) {
            return m.invoke(null, param, params[0], params[1]);
        }else if(m.getParameterCount() == 4) {
            return m.invoke(null, param, params[0], params[1], params[2]);
        }else if(m.getParameterCount() == 5) {
            return m.invoke(null, param, params[0], params[1], params[2], params[3]);
        }else {
            return null;
        }
    }

	private static Method match(List<LiquidParameter> objects, VarType type) {
		List<LiquidRequirement> allRequirements = new ArrayList<>();
        try {
            for (LiquidParameter param : objects) {
                allRequirements.add(param.getLiquidObject().getRequirement());
            }
        }catch(LiquidParameterException e) {
            e.printStackTrace();
        }
		
		Class<?>[] paramTypes = new Class<?>[allRequirements.size() + 1];
		paramTypes[0] = LiquidParameter.class;
		int i = 1;
		for(LiquidRequirement r : allRequirements) {
            paramTypes[i] = r.getClassType();
			i++;
		}
		
		String methodName = type.getMethodName();
		try {
			return VarConstructor.class.getMethod(methodName, paramTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			return null;
		}
	}
	
	private static List<Object> prepareContents(List<LiquidParameter> params) {
		List<Object> contents = new ArrayList<>();
		for (LiquidParameter param : params) {
			contents.add(param.getContent());
		}
		
		return contents;
	}

	private static List<LiquidParameter> toVarParameters(List<String> objects, boolean constant) throws LiquidException {
		List<LiquidParameter> params = new ArrayList<>();

		for (String o : objects) {
            params.add(ParameterUtils.fromStringToParam(o));
		}

		if(!constant) return params;
		for (LiquidParameter param : params) {
			if (!param.isUnereasable()) {
				throw new LiquidParameterException("value " + param.getLiquidObject().getPath() + " depends on other variables! Cannot instanciate constant!");
			}
		}

		return params;
	}
	
}
