package fr.badcookie20.liquid.core.consts;

import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;

import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This enum stores all the constants type
 * @author BadCookie20
 * @since InDev 3.0
 */
public enum VarType {

	ITEM("item", "buildItem", LiquidRequirement.REQUIRES_ITEMSTACK),
	LOC("location", "buildLoc", LiquidRequirement.REQUIRES_LOCATION),
    WORLD("world", "buildWorld", LiquidRequirement.REQUIRES_LOCATION),
	BOOLEAN("boolean", "buildBoolean", LiquidRequirement.REQUIRES_BOOLEAN);

    private String methodName;
    private String displayName;
    private LiquidRequirement type;

    VarType(String displayName, String methodName, LiquidRequirement type) {
        this.displayName = displayName;
        this.methodName = methodName;
        this.type = type;
    }

    public String displayName() {
        return this.displayName;
    }

    public LiquidParameter buildConstant(List<String> objects, String constName) throws LiquidException {
    	return VarConstructor.build(objects, constName, this, true);
    }

    public LiquidParameter buildVar(List<String> objects, String constName) throws LiquidException {
        return VarConstructor.build(objects, constName, this, false);
    }
    public String getMethodName() {
    	return this.methodName;
    }

    public static boolean isVarType(String type) {
        for(VarType t : values()) {
            if(type.equals(t.displayName)) {
                return true;
            }
        }

        return false;
    }

    public static boolean startWithVarType(String full) {
        for(VarType t : values()) {
            if(full.startsWith(t.displayName)) {
                return true;
            }
        }

        return false;
    }

    public static VarType getVarType(String line) {
        for(VarType t : values()) {
            if(line.equals(t.displayName)) {
                return t;
            }
        }

        return null;
    }

    public LiquidRequirement getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return this.displayName;
    }

}
