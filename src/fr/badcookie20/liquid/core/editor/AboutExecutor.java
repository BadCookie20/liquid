package fr.badcookie20.liquid.core.editor;

import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This command allow the player to get general information about a micro-plugin without accessing the file
 * @author BadCookie20
 * @since Beta 1.0
 */
public class AboutExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You can not get informations about a micro-plugin when you're not a player");
            return false;
        }

        Player p = (Player) sender;
        EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

        try {
			if (editionPlayer == null) {
				throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
			}

			if (!editionPlayer.isEditing()) {
				p.sendMessage(ChatColor.RED + "You cannot get information about any micro-plugin if you didn't invoke /edit!");
				return false;
			}

			MicroPlugin microPlugin = editionPlayer.getEditing();
			
			microPlugin.sendInfos(p);
			
			return true;
		} catch (LiquidEditionException e) {
            p.sendMessage(ChatColor.RED + "An unexpected error happened while trying to get the infos about the micro-plugin");
            e.printStackTrace();
		}
		
		return true;
	}
}
