package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.adder.*;
import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This class is the /add command executor
 * @author BadCookie20
 * @since InDev 3.0
 */
public class AddExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        try {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You can not add elements to a micro-plugin when you're not a player");
                return false;
            }

            Player p = (Player) sender;
            EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

            if (editionPlayer == null) {
                throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
            }

            if (!editionPlayer.isEditing()) {
                p.sendMessage(ChatColor.RED + "You cannot edit any micro-plugin if you didn't invoke /edit!");
                return false;
            }

            MicroPlugin microPlugin = editionPlayer.getEditing();

            if(editionPlayer.isInAdvancedEdition()) {
                p.sendMessage(ChatColor.RED + "You cannot add an element until you don't have completed the current edition!");
                return false;
            }

            p.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + "This command is deprecated, please use /gui instead");

            if (args.length == 0 || args[0] == null) {
                p.sendMessage(ChatColor.RED + "Usage : /add [cmd|event|const|property] [args]");
                return false;
            }

            if(args.length < 3 || args[1] == null || args[2] == null) {
                p.sendMessage(ChatColor.RED + "Usage : /add [cmd|event] [name] [actions]");
                return false;
            }

            if (args[0].equals("var")) {
                return new VarAdderSystem().apply(args, p, editionPlayer, microPlugin);
            }

            if (args[0].equals("cmd")) {
                return new CommandAdderSystem().apply(args, p, editionPlayer, microPlugin);
            }

            if (args[0].equals("event")) {
                return new EventAdderSystem().apply(args, p, editionPlayer, microPlugin);
            }

            if(args[0].equals("struct")) {
                return new StructAdderSystem().apply(args, p, editionPlayer, microPlugin);
            }

            if(args[0].equals("property")) {
                return new PropertyAdderSystem().apply(args, p, editionPlayer, microPlugin);
            }

            p.sendMessage(ChatColor.RED + "Usage : /add [cmd|event|const|struct|property] [actions]");

            return true;
        }catch(LiquidEditionException e) {
            e.printStackTrace();
        }

        return true;
    }
}
