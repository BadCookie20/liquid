package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This command allows the player to cancel all the modifications applied to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class CancelExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		try {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "You can not add elements to a micro-plugin when you're not a player");
				return false;
			}
			
			Player p = (Player) sender;
			EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);
			
			if (editionPlayer == null) {
				throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
			}
			
			if (!editionPlayer.isEditing()) {
				p.sendMessage(ChatColor.RED + "You cannot erase the edition of any micro-plugin if you didn't invoke /edit!");
				return false;
			}

			if(editionPlayer.isInAdvancedEdition()) {
				p.sendMessage(ChatColor.RED + "You cannot erase the edition data until you don't have completed the current edition!");
				return false;
			}
			
			editionPlayer.emptyEdition();
			
			return true;
		}catch(LiquidEditionException e) {
			e.printStackTrace();
		}
		
		return true;
	}

}
