package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.LiquidPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * This command allows anyone to create any micro-plugin
 * @author BadCookie20
 * @since Beta 1.1
 */
public class CreateExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 0 || args[0] == null) {
            sender.sendMessage(ChatColor.RED + "Usage : /create [name]");
            return false;
        }

        if(sender instanceof Player && EditionPlayer.getEditionPlayer((Player) sender).isInAdvancedEdition()) {
            sender.sendMessage(ChatColor.RED + "You cannot create a micro-plugin until you don't have completed the current edition!");
            return false;
        }

        String name = args[0];

        for(MicroPlugin microPlugin : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            if(microPlugin.getName().equalsIgnoreCase(name)) {
                sender.sendMessage(ChatColor.RED + "A micro-plugin with name " + name + " already exists!");
                return false;
            }
        }

        try {
            File f = new File(LiquidPlugin.getInstance().getDataFolder().getAbsolutePath() + "\\" + name + ".lmp");
            boolean c = f.createNewFile();
            if(!c) throw new IOException();
        }catch(IOException ex) {
            sender.sendMessage(ChatColor.RED + "An unexpected error happened while trying to create the micro-plugin file!");
            return false;
        }

        sender.sendMessage(ChatColor.GREEN + "Successfully created the new micro-plugin file! Please reload the server so it's loaded. You may then edit it by doing /edit on " + name);

        return true;
    }
}
