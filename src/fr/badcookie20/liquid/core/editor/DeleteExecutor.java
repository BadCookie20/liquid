package fr.badcookie20.liquid.core.editor;

import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This command allows anyone which invoked /edit to delete a micro-plugin
 * @author BadCookie20
 * @since Beta 1.1
 */
public class DeleteExecutor implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        try {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You can not delete a micro-plugin when you're not a player");
                return false;
            }

            Player p = (Player) sender;
            EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

            if (editionPlayer == null) {
                throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
            }

            if(editionPlayer.isInAdvancedEdition()) {
                p.sendMessage(ChatColor.RED + "You cannot delete a micro-plugin until you don't have completed the current edition!");
                return false;
            }

            if (!editionPlayer.isEditing()) {
                p.sendMessage(ChatColor.RED + "You cannot delete any micro-plugin if you didn't invoke /edit!");
                return false;
            }

            MicroPluginUtils.deleteMicroPlugin(p, editionPlayer);

            return true;
        }catch(LiquidEditionException ex) {
            ex.printStackTrace();
        }
        return true;
    }
}
