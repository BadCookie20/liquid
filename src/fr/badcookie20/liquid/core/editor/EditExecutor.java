package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This class represents the command /edit executor
 * @author BadCookie20
 * @since InDev 3.0
 */
public class EditExecutor implements CommandExecutor {

    private static String IND_PREFIX = ChatColor.GRAY + "" + ChatColor.ITALIC;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String message, String[] args) {
        try {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Only a player can modify a micro-plugin!");
                return false;
            }

            Player p = (Player) sender;
            EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

            if (editionPlayer == null) {
                throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
            }

            if(args.length == 0 || args[0] == null) {
                p.sendMessage(ChatColor.RED + "Usage : /edit [on|off]");
                return false;
            }

            if (args[0].equals("on")) {
                if(args.length == 1 || args[1] == null) {
                    p.sendMessage(ChatColor.RED + "Usage : /edit on [micro-plugin_name]");
                    return false;
                }

                MicroPlugin microPlugin = MicroPluginUtils.getMicroPluginByName(args[1]);

                if (editionPlayer.isEditing()) {
                    p.sendMessage(ChatColor.RED + "You are already editing a micro-plugin!");
                    return false;
                }

                if (microPlugin == null) {
                    p.sendMessage(ChatColor.RED + "Couldn't find micro-plugin with name " + args[1] + "!");
                    p.sendMessage(ChatColor.RED + "If you want to create a micro-plugin, enter /create [name]");
                    return false;
                }

                if(microPlugin.editOnlyOp() && !p.isOp()) {
                    p.sendMessage(ChatColor.RED + "You cannot edit this micro-plugin because you are not op!");
                    return false;
                }

                if(microPlugin.isIgnored()) {
                    p.sendMessage(ChatColor.RED + "The micro-plugin is currently ignored because -ignore is set to true; you cannot edit it for the moment");
                    return false;
                }

                editionPlayer.turnOnEditing(microPlugin);
                p.sendMessage(ChatColor.GREEN + "You are now editing the " + microPlugin.getName() + " micro-plugin!");
                p.sendMessage(IND_PREFIX + "Use /gui to open a gui to edit your micro-plugin, or use the /add command");
                return true;
            }

            if (args[0].equals("off")) {
                if(editionPlayer.isInAdvancedEdition()) {
                    p.sendMessage(ChatColor.RED + "You cannot exit the edition until you don't have completed the current edition!");
                    return false;
                }

                if (!editionPlayer.isEditing()) {
                    p.sendMessage(ChatColor.RED + "You are not editing!");
                    return false;
                }

                editionPlayer.turnOffEditing();
                return true;
            }

            p.sendMessage(ChatColor.RED + "Usage : /edit [on|off]");

            return true;
        }catch(LiquidEditionException e) {
            e.printStackTrace();
        }

        return true;
    }
}
