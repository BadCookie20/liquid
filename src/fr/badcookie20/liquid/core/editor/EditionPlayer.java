package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.IndividualScoreboard;
import fr.badcookie20.liquid.core.utils.Logger;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents all the connected players as EditionPlayers
 * @author BadCookie20
 * @since InDev 3.0
 */
public class EditionPlayer {

    public static final ItemStack GUI_ITEM = BukkitUtils.createItemStack(Material.BOOK, ChatColor.GREEN + "Open GUI", Collections.singletonList(ChatColor.AQUA + "Opens the GUI"));
    private static List<EditionPlayer> players;

    static{
        players = new ArrayList<>();
    }

    private Player p;
    private MicroPlugin editing;
    private boolean inAdvancedEdition;
    private List<String> addedLines;
    private IndividualScoreboard scoreboard;

    private EditionPlayer(Player p) {
        this.p = p;
        this.addedLines = new ArrayList<>();
        this.scoreboard = new IndividualScoreboard(this.p, ChatColor.GREEN + "Editing...");
        this.inAdvancedEdition = false;

        players.add(this);
    }

    public Player getPlayer() {
        return this.p;
    }

    public boolean isEditing() {
        return editing != null;
    }

    public void turnOffEditing() {
        boolean exists = this.editing.getFile().exists();
        boolean modifiedSomething = !this.addedLines.isEmpty();
        List<String> linesToRemove = new ArrayList<>();
        try {
            if(exists) {
                for (String line : this.addedLines) {
                    if(line.startsWith(":")) {
                        line = new StringBuilder(line).deleteCharAt(0).toString();
                        String lineToRemove = MicroPluginUtils.getLineToRemoveIfNeeded(this.editing, line);
                        if(lineToRemove != null) linesToRemove.add(lineToRemove);
                    }
                }
                MicroPluginUtils.removeLines(this.editing, linesToRemove);

                for(String line : this.addedLines) {
                    if(line.startsWith(":")) {
                        line = new StringBuilder(line).deleteCharAt(0).toString();
                        MicroPluginUtils.addLine(this.editing, line);
                    }else{
                        MicroPluginUtils.addLine(this.editing, line);
                    }
                }
            }

            this.addedLines.clear();

            if(this.editing.showScoreboard()) {
                this.scoreboard.end();
                this.editing = null;
            }

            if(exists) {
                if(modifiedSomething) {
                    this.p.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + "Saved all the lines in the micro-plugin");
                }else{
                    this.p.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + "You didn't modify anything");
                }
            }else{
                this.p.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + "This micro-plugin does not exist anymore");
            }
            this.p.getInventory().remove(GUI_ITEM);
            this.p.sendMessage(ChatColor.GREEN + "You may now reload the server to apply the modifications!");
        }catch(LiquidEditionException e) {
            e.printStackTrace();
        }
    }

    public void turnOnEditing(MicroPlugin microPlugin) {
        this.editing = microPlugin;
        if(this.editing.showScoreboard()) {
            this.scoreboard.make();
            this.scoreboard.setLine(0, ChatColor.AQUA + "Editing " + ChatColor.GOLD + microPlugin.getName());
            this.scoreboard.setLine(1, ChatColor.GOLD + "-------------");
            this.scoreboard.setLine(2, ChatColor.AQUA + "Vars: " + ChatColor.GOLD + microPlugin.getAllVars().size());
            this.scoreboard.setLine(3, ChatColor.AQUA + "Events: " + ChatColor.GOLD + microPlugin.getAllLiquidEvents().size());
            this.scoreboard.setLine(4, ChatColor.AQUA + "Commands: " + ChatColor.GOLD + microPlugin.getAllLiquidCommands().size());
            this.scoreboard.setLine(5, ChatColor.AQUA + "Structs: " + ChatColor.GOLD + microPlugin.getAllStructs().size());
            this.scoreboard.setLine(6, ChatColor.GOLD + "-------------");
            this.scoreboard.setLine(7, ChatColor.AQUA + "Modifications: " + ChatColor.GOLD + "0");
        }else{
            this.p.sendMessage(ChatColor.RED + "The parameter -showsc is set to false. The scoreboard is not shown");
        }

        if(this.p.getInventory().contains(GUI_ITEM)) return;
        this.p.getInventory().addItem(GUI_ITEM);
    }

    public MicroPlugin getEditing() {
        return this.editing;
    }

    public void addLine(String line) {
        this.p.sendMessage(ChatColor.GRAY + "Successfully added line " + line);
        this.addedLines.add(line);
        this.scoreboard.setLine(7, ChatColor.AQUA + "Modifications: " + ChatColor.GOLD + addedLines.size());
    }

    public static List<EditionPlayer> getPlayers() {
        return players;
    }

    public static EditionPlayer getEditionPlayer(Player p) {
        for(EditionPlayer editionPlayer : players) {
            if(editionPlayer.getPlayer().equals(p)) {
                return editionPlayer;
            }
        }

        Logger.logWarning("The player " + p.getName() + " is not registered as an edition player");
        return null;
    }

    public void setInAdvancedEdition() {
        this.inAdvancedEdition = true;
    }

    public void setNotInAdvancedEdition() {
        this.inAdvancedEdition = false;
    }

    public boolean isInAdvancedEdition() {
        return inAdvancedEdition;
    }

    public static EditionPlayer registerPlayer(Player p) {
        return new EditionPlayer(p);
    }

    public static void removePlayer(EditionPlayer p) {
        if(p.isEditing()) p.turnOffEditing();
        players.remove(p);
    }

    public static void registerOnlinePlayers() {
        for(Player p : Bukkit.getOnlinePlayers()) {
            registerPlayer(p);
        }
    }

    public static void removeAllPlayers() {
        for(EditionPlayer p : players) {
            if(p.isEditing()) p.turnOffEditing();
        }

        players.clear();
    }

	public void emptyEdition() {
		this.addedLines.clear();
        this.p.sendMessage(ChatColor.GREEN + "All edition data has been deleted");
        this.scoreboard.setLine(7, ChatColor.AQUA + "Lines added: " + ChatColor.GOLD + "0");
	}
}

