package fr.badcookie20.liquid.core.editor;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.gui.HomeInventory;
import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This command allows the player to open the GUI to edit micro-plugins
 * @author BadCookie20
 * @since Beta 1.3
 */
public class GUIExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        return openGui(sender);
    }

    public static boolean openGui(CommandSender sender) {
        try {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You can not add elements to a micro-plugin when you're not a player");
                return false;
            }

            Player p = (Player) sender;
            EditionPlayer editionPlayer = EditionPlayer.getEditionPlayer(p);

            if (editionPlayer == null) {
                throw new LiquidEditionException(p.getDisplayName() + " is not registered as an EditionPlayer!");
            }

            if (!editionPlayer.isEditing()) {
                p.sendMessage(ChatColor.RED + "You cannot edit any micro-plugin if you didn't invoke /edit!");
                return false;
            }

            if(editionPlayer.isInAdvancedEdition()) {
                p.sendMessage(ChatColor.RED + "You cannot open the gui until don't you have completed the current edition!");
                return false;
            }

            MicroPlugin microPlugin = editionPlayer.getEditing();

            editionPlayer.getPlayer().openInventory(HomeInventory.getInstance().get(microPlugin));
        }catch(LiquidEditionException e) {
            e.printStackTrace();
        }

        return true;
    }
}
