package fr.badcookie20.liquid.core.editor.adder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.entity.Player;

/**
 * This class is the main abstract class for adding content to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public interface AdderSystem {

    boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin);

    void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin);

    void end(EditionPlayer editionPlayer);

}
