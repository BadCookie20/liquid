package fr.badcookie20.liquid.core.editor.adder;

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.gui.ActionsInventory;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is used to add a command statement to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class CommandAdderSystem implements AdderSystem {

    @Override
    public boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        String cmdName = args[1];
        List<String> actions = new ArrayList<>();
        actions.addAll(Arrays.asList(args).subList(2, args.length));

        if(MicroPluginUtils.isLiquidCommandAlreadyRegistered(cmdName, microPlugin)) {
            p.sendMessage(ChatColor.RED + "Error while defining new command (there is already a command with name " + cmdName + ")");
            return false;
        }

        String line = "def cmd " + cmdName + " ";
        for(String a : actions) {
            line+=a;
            line+=" ";
        }

        editionPlayer.addLine(line);
        p.sendMessage(ChatColor.GREEN + "Added command " + cmdName);
        return true;
    }

    @Override
    public void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.sendMessage(ChatColor.GREEN + "Enter the name of the command:");
        String cmdName = NextUtils.getNextMessageOf(p, true);
        
        if(MicroPluginUtils.isLiquidCommandAlreadyRegistered(cmdName, microPlugin)) {
            p.sendMessage(ChatColor.RED + "Error while defining new command (there is already a command with name " + cmdName + ")");
            end(editionPlayer);
            return;
        }
        
        p.sendMessage(ChatColor.GREEN + "Choose the actions you want to involve with this command");
        
        String actions = ActionsInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);
        
        if(actions == null) {
        	p.sendMessage(ChatColor.RED + "Illegal actions");
        	end(editionPlayer);
        	return;
        }
        
        editionPlayer.addLine("def cmd " + cmdName + actions);
        
        end(editionPlayer);
    }

    @Override
    public void end(EditionPlayer editionPlayer) {
    	editionPlayer.setNotInAdvancedEdition();
        BukkitUtils.enableAsyncCatcher();
    }
}
