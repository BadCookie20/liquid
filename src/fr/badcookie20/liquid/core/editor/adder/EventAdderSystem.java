package fr.badcookie20.liquid.core.editor.adder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.gui.ActionsInventory;
import fr.badcookie20.liquid.core.editor.gui.EventsInventory;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is used to add an event statement to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class EventAdderSystem implements AdderSystem {

    @Override
    public boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        String eventName = args[1];
        List<String> actions = new ArrayList<>();
        actions.addAll(Arrays.asList(args).subList(2, args.length));

        if(MicroPluginUtils.getEventInString(eventName) == null) {
            p.sendMessage(ChatColor.RED + "Error while adding an event listener (unknown event " + eventName + ")");
            return false;
        }

        StringBuilder insertLine = new StringBuilder();
        insertLine.append(eventName);
        insertLine.append(" ");

        for(String a : actions) {
            insertLine.append(a);
            insertLine.append(" ");
        }

        editionPlayer.addLine(insertLine.toString());
        p.sendMessage(ChatColor.GREEN + "Added event " + eventName + " listener");
        return true;
    }

    @Override
    public void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.sendMessage(ChatColor.GREEN + "Choose the event you want to listen to");
        String eventName = EventsInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);
        
        if(eventName == null) {
            p.sendMessage(ChatColor.RED + "Illegal event type");
        	end(editionPlayer);
        	return;
        }
        
        p.sendMessage(ChatColor.GREEN + "Choose the actions you want to involve with this event");
        String actions = ActionsInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);
        
        if(actions == null) {
        	p.sendMessage(ChatColor.RED + "Illegal actions");
        	end(editionPlayer);
        	return;
        }
        
        editionPlayer.addLine(eventName + " " + actions);

        end(editionPlayer);
    }

    @Override
    public void end(EditionPlayer editionPlayer) {
		editionPlayer.setNotInAdvancedEdition();
        BukkitUtils.enableAsyncCatcher();
	}
}
