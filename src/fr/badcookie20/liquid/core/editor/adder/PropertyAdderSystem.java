package fr.badcookie20.liquid.core.editor.adder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.gui.PropertiesInventory;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginProperty;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * This class is used to add a structure statement to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class PropertyAdderSystem implements AdderSystem {

    @Override
    public boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        String propertyName = args[1];
        String value = args[2];

        if(MicroPluginProperty.getPropertyByName(propertyName) == null) {
            p.sendMessage(ChatColor.RED + "Couldn't resolve property " + propertyName);
            return false;
        }

        if(!value.equals("true") && !value.equals("false")) {
            p.sendMessage(ChatColor.RED + "Couldn't resolve boolean " + value);
            return false;
        }

        editionPlayer.addLine(":" + propertyName + ":" + value);

        p.sendMessage(ChatColor.GREEN + "Modified property " + propertyName + " to value " + value);

        return true;
    }

    @Override
    public void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.sendMessage(ChatColor.GREEN + "Choose the property you want to modify:");
        String property = PropertiesInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);

        if(property == null) {
            p.sendMessage(ChatColor.RED + "Illegal constant type");
            end(editionPlayer);
            return;
        }

        p.sendMessage(ChatColor.GREEN + "Enter the new value of the property:");
        String newValue = NextUtils.getNextMessageOf(p, true);

        editionPlayer.addLine(":" + property + ":" + newValue);

        end(editionPlayer);
    }

    @Override
    public void end(EditionPlayer editionPlayer) {
        editionPlayer.setNotInAdvancedEdition();
        BukkitUtils.enableAsyncCatcher();
    }


}
