package fr.badcookie20.liquid.core.editor.adder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.gui.RequirementsInventory;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * This class is used to add a structure statement to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class StructAdderSystem implements AdderSystem {

    @Override
    public boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        String structName = args[1];
        String structType = args[2];
        String structParams = args[3];

        if(MicroPluginUtils.getStruct(structName) != null) {
            p.sendMessage(ChatColor.RED + "Error while defining new structure (there is already a structure with name " + structName);
            return false;
        }

        if(StructType.TypeList.getStructType(structType) == null) {
            p.sendMessage(ChatColor.RED + "Error while defining new structure (unknown type " + structType + ")");
            return false;
        }

        String line = "def struct " + structType + " " + structName + " {" + structParams + "}";
        editionPlayer.addLine(line);
        p.sendMessage(ChatColor.GREEN + "Added structure " + structName);

        return true;
    }

    @Override
    public void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.sendMessage(ChatColor.GREEN + "Enter the name of the struct you want to create:");
        String name = NextUtils.getNextMessageOf(p, true);

        if(MicroPluginUtils.getStruct(name) != null) {
            p.sendMessage(ChatColor.RED + "Error while defining new structure (there is already a structure with name " + name);
            end(editionPlayer);
            return;
        }

        p.sendMessage(ChatColor.GOLD + "There is only one type of structures available (type=players)");
        p.sendMessage(ChatColor.GREEN + "Choose the properties of the structure:");
        String properties = RequirementsInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);

        if(properties == null) {
            p.sendMessage(ChatColor.RED + "Illegal properties");
            end(editionPlayer);
            return;
        }

        editionPlayer.addLine("def struct player " + name + " " + properties);

        end(editionPlayer);
    }

    @Override
    public void end(EditionPlayer editionPlayer) {
        editionPlayer.setNotInAdvancedEdition();
        BukkitUtils.enableAsyncCatcher();
    }
}
