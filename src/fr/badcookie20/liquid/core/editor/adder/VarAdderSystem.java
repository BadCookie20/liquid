package fr.badcookie20.liquid.core.editor.adder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.consts.VarType;
import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.editor.gui.VarTypeInventory;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * This class is used to add a constant statement to a micro-plugin
 * @author BadCookie20
 * @since Beta 1.3
 */
public class VarAdderSystem implements AdderSystem {

    @Override
    public boolean apply(String[] args, Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        String constName = args[1];
        String type = args[2];
        String value = args[3];

        if(MicroPluginUtils.isVarAlreadyRegistered(constName, microPlugin)) {
            p.sendMessage(ChatColor.RED + "Error while defining new constant (there is already a constant with name " + constName + ")");
            return false;
        }

        if(!VarType.isVarType(type)) {
            p.sendMessage(ChatColor.RED + "Error while defining new constant (unknown type " + type + ")");
            return false;
        }

        String line = "def " + type + " " + constName + " = " + value;
        editionPlayer.addLine(line);
        p.sendMessage(ChatColor.GREEN + "Added constant " + constName);

        return true;
    }

    @Override
    public void interactWithPlayer(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.closeInventory();
        p.sendMessage(ChatColor.GREEN + "Enter the name of the constant:");
        String constName = NextUtils.getNextMessageOf(p, true);

        if(MicroPluginUtils.isVarAlreadyRegistered(constName, microPlugin)) {
            p.sendMessage(ChatColor.RED + "Error while defining new constant (there is already a constant with name " + constName + ")");
            end(editionPlayer);
            return;
        }

        p.sendMessage(ChatColor.GREEN + "Choose the type of the constant");
        String type = VarTypeInventory.getInstance().handleChoosing(p, editionPlayer, microPlugin);

        if(type == null) {
            p.sendMessage(ChatColor.RED + "Illegal constant type");
            end(editionPlayer);
            return;
        }

        p.sendMessage(ChatColor.GREEN + "Enter the value of the constant:");
        String value = NextUtils.getNextMessageOf(p, true);

        editionPlayer.addLine("def " + type + " " + constName + " = " + value);

        end(editionPlayer);
    }

    @Override
    public void end(EditionPlayer editionPlayer) {
        editionPlayer.setNotInAdvancedEdition();
        BukkitUtils.enableAsyncCatcher();
    }

}
