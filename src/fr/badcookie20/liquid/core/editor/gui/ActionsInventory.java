package fr.badcookie20.liquid.core.editor.gui;

import fr.badcookie20.liquid.core.actions.LiquidAction;
import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import fr.badcookie20.liquid.core.utils.annotations.InTest;
import fr.badcookie20.liquid.core.utils.annotations.NeedsOptimization;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the actions inventory involved with the edition GUI
 * @author BadCookie20
 * @since Beta 1.3
 */
public class ActionsInventory extends GUIInventory {

	private static ActionsInventory instance;
    private static Inventory tempInventory;
    
    private static String DESC_PREFIX = ChatColor.AQUA + "";
	
    static {
    	tempInventory = Bukkit.createInventory(null, 18, ">Actions");
    	
    	for(LiquidAction a : LiquidAction.values()) {
    		List<LiquidRequirement> requirements = a.getRequirements();
    		LiquidRequirement instanceRequirement = a.getInstanceRequirement();
    		
    		List<String> lores = new ArrayList<>();

			String description = ChatColor.GREEN + a.getDescription();
    		String loreRequirements = DESC_PREFIX + "Arguments: " + ChatColor.GRAY + (requirements.isEmpty() ? "none":LangUtils.list(requirements, ','));
    		String loreInstance = DESC_PREFIX + "Instance requirement: " + ChatColor.GRAY + (instanceRequirement == null ? "none":instanceRequirement.toString());

            lores.addAll(LangUtils.toListFromString(description));
    		lores.addAll(LangUtils.toListFromString(loreRequirements));
    		lores.add(loreInstance);
    		
    		tempInventory.addItem(BukkitUtils.createItemStack(Material.SLIME_BLOCK,
    				ChatColor.GREEN + ">" + ChatColor.GOLD + a.getActionName(),
    				lores));
    	}

        tempInventory.setItem(tempInventory.getSize() - 1, BukkitUtils.createItemStack(Material.REDSTONE, ChatColor.RED + "Exit actions editor", null));
    }
    
	public ActionsInventory() {
		super(tempInventory);
		
		instance = this;
	}

	@Override
	public Inventory get(MicroPlugin microPlugin) {
		return this.bukkitInventory;
	}
	
	public static ActionsInventory getInstance() {
		return instance;
	}
	
	@InTest
	@NeedsOptimization
	@Override
	public String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
		String actions = "";
		
		boolean stop = false;
		boolean first = true;
		do{
			if(!first) {
                p.sendMessage(ChatColor.GREEN + "Click on an action if you want to add another one");
            }
			String action = handleSingleActionChoosing(p, editionPlayer, microPlugin, first);
			
			if(action == null || action.equals("null")) {
				stop = true;
				if(first) return null;
				return actions;
			}else{
				actions+=" " + action;
			}
				
			first = false;
		}while(!stop);
		
		return actions;
	}
	
	private String handleSingleActionChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin, boolean first) {
		p.openInventory(ActionsInventory.getInstance().get(microPlugin));

		String s = "";
		
		ItemStack item = NextUtils.getNextClickedItemOf(p);
		if(!MicroPluginUtils.hasClickedAnAction(item)) {
			if(first) {
				p.sendMessage(ChatColor.RED + "You didn't click on any action! Stopping the current edition...");
			}
			return null;
		}
		
		
		String actionName = new StringBuilder(item.getItemMeta().getDisplayName()).delete(0, 5).toString();

        if(actionName.contains("Exit")) {
            return null;
        }

		LiquidAction action = LiquidAction.findActionByName(actionName);
		
		if(action == null) {
			p.sendMessage(ChatColor.RED + "Unknown action " + actionName);
			return null;
		}
		
		s+=actionName + "(";
		
		if(action.getRequirements().isEmpty()) {
			s+=")";
		}else{
			int i = action.getRequirements().size();
			int j = 0;
			for(LiquidRequirement requirement : action.getRequirements()) {
				j++;
				p.sendMessage(ChatColor.GREEN + "Type the value who want to use for the argument which requires " + requirement.toString());
				String input = NextUtils.getNextMessageOf(p, true);
				if(input == null || input.isEmpty()) {
					p.sendMessage(ChatColor.RED + "Illegal argument!");
					return null;
				}
				s+=(input + (i == j ? ")":","));
			}
		}
		
		if(!action.isStatic()) {
			s+="->";
            LiquidRequirement requirement = action.getInstanceRequirement();
			p.sendMessage(ChatColor.GREEN + "Type the value you want to use for the instance which requires " + requirement.toString() +
                    (requirement == LiquidRequirement.REQUIRES_STRING ? "(no \" needed)":""));
			String input = NextUtils.getNextMessageOf(p, true);
			if(input == null || input.isEmpty()) {
				p.sendMessage(ChatColor.RED + "Illegal argument!");
				return null;
			}

            if(requirement == LiquidRequirement.REQUIRES_STRING) {
                input = "\"" + input + "\"";
            }

			s+=input;
		}
		
		p.sendMessage(ChatColor.GREEN + "Type a condition (without the brackets) if you want to add one to the action or type none if you don't want to use any");
		String condition = NextUtils.getNextMessageOf(p, true);
		
		if(condition == null || condition.isEmpty()) {
			p.sendMessage(ChatColor.RED + "Illegal argument!");
			return null;
		}
		if(!condition.equals("none")) s+="[" + condition + "]";
		
		return s;
	}

}
