package fr.badcookie20.liquid.core.editor.gui;

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import fr.badcookie20.liquid.core.utils.annotations.InTest;
import fr.badcookie20.liquid.core.utils.annotations.NeedsOptimization;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the events inventory involved with the edition GUI
 * @author BadCookie20
 * @since Beta 1.3
 */
public class EventsInventory extends GUIInventory {

	private static EventsInventory instance;
    private static Inventory tempInventory;
    
    private static String DESC_PREFIX = ChatColor.AQUA + "";
	
    static {
    	tempInventory = Bukkit.createInventory(null, 18, ">Events");
    	
    	for(LiquidEvent.EventList a : LiquidEvent.EventList.values()) {
    		tempInventory.addItem(BukkitUtils.createItemStack(Material.BOOK,
    				ChatColor.GREEN + ">" + a.getName(), 
    				null));
    	}
    }
    
	public EventsInventory() {
		super(tempInventory);
		
		instance = this;
	}

	@Override
	public Inventory get(MicroPlugin microPlugin) {
		return this.bukkitInventory;
	}
	
	public static EventsInventory getInstance() {
		return instance;
	}
	
	@InTest
	@NeedsOptimization
	@Override
	public String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
		p.openInventory(EventsInventory.getInstance().get(microPlugin));
		
		ItemStack item = NextUtils.getNextClickedItemOf(p);
		if(!MicroPluginUtils.hasClickedAnEvent(item)) {
			p.sendMessage(ChatColor.RED + "You didn't click any event! Stopping the current edition...");
			return null;
		}

		return new StringBuilder(item.getItemMeta().getDisplayName()).delete(0, 3).toString();
	}

}
