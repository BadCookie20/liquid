package fr.badcookie20.liquid.core.editor.gui;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * This class is the base class for all the inventories of the edition GUI
 * @author BadCookie20
 * @since Beta 1.3
 */
public abstract class GUIInventory {

    protected Inventory bukkitInventory;

    public GUIInventory(Inventory bukkitInventory) {
        this.bukkitInventory = bukkitInventory;
    }

    public abstract Inventory get(MicroPlugin microPlugin);

    public abstract String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin);
}
