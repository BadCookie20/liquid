package fr.badcookie20.liquid.core.editor.gui;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

/**
 * This class represents the Home inventory
 * @author BadCookie20
 * @since Beta 1.3
 */
public class HomeInventory extends GUIInventory {

    private static HomeInventory instance;
    private static Inventory tempInventory;

    private static String NAME_PREFIX = ChatColor.GREEN + "";
    private static String CAUTION_NAME_PREFIX = ChatColor.RED + "";
    private static String DESC_PREFIX = ChatColor.AQUA + "";

    static {
        tempInventory = Bukkit.createInventory(null, 36, ">editor");
        tempInventory.setItem(0, BukkitUtils.createItemStack(Material.PAPER, NAME_PREFIX + "About the micro-plugin",
                Arrays.asList(DESC_PREFIX + "Allows you to get general information",
                        DESC_PREFIX + "about the micro-plugin",
                        DESC_PREFIX + "(alternative command: /about)")));
        tempInventory.setItem(3, BukkitUtils.createItemStack(Material.EMERALD, NAME_PREFIX + "Close GUI", null));
        tempInventory.setItem(4, BukkitUtils.createItemStack(Material.REDSTONE, CAUTION_NAME_PREFIX + "Exit edition",
                Arrays.asList(DESC_PREFIX + "Allows you to quit the edition mode and",
                        DESC_PREFIX + "save the modifications",
                        DESC_PREFIX + "(alternative command: /edit off)")));
        tempInventory.setItem(5, BukkitUtils.createItemStack(Material.CACTUS, NAME_PREFIX + "Erase modifications",
                Arrays.asList(DESC_PREFIX + "Allows you to cancel all the modifications",
                        DESC_PREFIX + "(alternative command: /cancel)")));
        tempInventory.setItem(8, BukkitUtils.createItemStack(Material.REDSTONE_BLOCK, CAUTION_NAME_PREFIX + "Delete micro-plugin",
        		Arrays.asList(DESC_PREFIX + "Warning! You cannot cancel this action!",
                        DESC_PREFIX + "You have to be op to do this", ChatColor.AQUA + "(alternative command: /delete)")));

        tempInventory.setItem(20, BukkitUtils.createItemStack(Material.GRASS, NAME_PREFIX + "Add event",
                Arrays.asList(DESC_PREFIX + "Allows you to add an event to the",
                        DESC_PREFIX + "micro-plugin",
                        DESC_PREFIX + "(alternative command: /add event)")));
        tempInventory.setItem(21, BukkitUtils.createItemStack(Material.COMMAND, NAME_PREFIX + "Add command",
                Arrays.asList(DESC_PREFIX + "Allows you to add a command to the",
                        DESC_PREFIX + "micro-plugin",
                        DESC_PREFIX + "(alternative command: /add cmd)")));
        tempInventory.setItem(22, BukkitUtils.createItemStack(Material.REDSTONE_TORCH_ON, NAME_PREFIX + "Add variable",
                Arrays.asList(DESC_PREFIX + "Allows you to add a variable to the",
                        DESC_PREFIX + "micro-plugin",
                        DESC_PREFIX + "(alternative command: /add var)")));
        tempInventory.setItem(23, BukkitUtils.createItemStack(Material.SIGN, NAME_PREFIX + "Add structure",
                Arrays.asList(DESC_PREFIX + "Allows you to add a structure to the",
                        DESC_PREFIX + "micro-plugin",
                        DESC_PREFIX + "(alternative command: /add struct)")));
        tempInventory.setItem(24, BukkitUtils.createItemStack(Material.DAYLIGHT_DETECTOR, NAME_PREFIX + "Modify property",
                Arrays.asList(DESC_PREFIX + "Allows you to modify the value of a",
                        DESC_PREFIX + "property of the micro-plugin",
                        DESC_PREFIX + "(alternative command: /add property)")));
    }

    public HomeInventory() {
        super(tempInventory);

        instance = this;
    }

    @Override
    public Inventory get(MicroPlugin microPlugin) {
        if(this.bukkitInventory.getName().equals(">" + microPlugin.getName())) return this.bukkitInventory;
        return BukkitUtils.renameInventory(this.bukkitInventory, ">" + microPlugin.getName());
    }

    public static HomeInventory getInstance() {
        return instance;
    }

    @Override
    public String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        return null;
    }
}
