package fr.badcookie20.liquid.core.editor.gui;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.entity.Player;

/**
 * This class allows the gui system to go back to another inventory
 * @author BadCookie20
 * @since Beta 1.3
 */
public class InventoryRelation {

    private GUIInventory previousInventory;

    public InventoryRelation(GUIInventory previousInventory) {
        this.previousInventory = previousInventory;
    }

    public void openPreviousInventory(MicroPlugin microPlugin, Player p) {
        p.openInventory(previousInventory.get(microPlugin));
    }

}
