package fr.badcookie20.liquid.core.editor.gui;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginProperty;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import fr.badcookie20.liquid.core.utils.annotations.InTest;
import fr.badcookie20.liquid.core.utils.annotations.NeedsOptimization;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * This class represents the properties inventory involved with the edition GUI
 * @author BadCookie20
 * @since Beta 1.3
 */
public class PropertiesInventory extends GUIInventory {

    private static PropertiesInventory instance;
    private static Inventory tempInventory;

    static {
        tempInventory = Bukkit.createInventory(null, 9, ">Properties");

        for(MicroPluginProperty p : MicroPluginProperty.values()) {
            tempInventory.addItem(BukkitUtils.createItemStack(Material.BLAZE_ROD,
                    ChatColor.GREEN + ">" + ChatColor.GOLD + p.toString(),
                    null));
        }
    }

    public PropertiesInventory() {
        super(tempInventory);

        instance = this;
    }

    @Override
    public Inventory get(MicroPlugin microPlugin) {
        return this.bukkitInventory;
    }

    public static PropertiesInventory getInstance() {
        return instance;
    }

    @InTest
    @NeedsOptimization
    @Override
    public String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
        p.openInventory(PropertiesInventory.getInstance().get(microPlugin));

        ItemStack item = NextUtils.getNextClickedItemOf(p);
        if(!MicroPluginUtils.hasClickedAProperty(item)) {
            p.sendMessage(ChatColor.RED + "You didn't click any property! Stopping the current edition...");
            return null;
        }

        return new StringBuilder(item.getItemMeta().getDisplayName()).delete(0, 5).toString();
    }

}
