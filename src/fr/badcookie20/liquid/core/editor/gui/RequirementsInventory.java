package fr.badcookie20.liquid.core.editor.gui;

import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import fr.badcookie20.liquid.core.utils.NextUtils;
import fr.badcookie20.liquid.core.utils.annotations.InTest;
import fr.badcookie20.liquid.core.utils.annotations.NeedsOptimization;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the actions inventory involved with the edition GUI
 * @author BadCookie20
 * @since Beta 1.3
 */
public class RequirementsInventory extends GUIInventory {

	private static RequirementsInventory instance;
    private static Inventory tempInventory;

    private static String DESC_PREFIX = ChatColor.AQUA + "";

    static {
    	tempInventory = Bukkit.createInventory(null, 18, ">Type");

    	for(LiquidRequirement r : LiquidRequirement.values()) {
    		tempInventory.addItem(BukkitUtils.createItemStack(Material.COMPASS,
    				ChatColor.GREEN + ">" + ChatColor.GOLD + r.toString(),
    				null));
    	}

        tempInventory.setItem(tempInventory.getSize() - 1, BukkitUtils.createItemStack(Material.REDSTONE, ChatColor.RED + "Exit properties editor", null));
    }

	public RequirementsInventory() {
		super(tempInventory);
		
		instance = this;
	}

	@Override
	public Inventory get(MicroPlugin microPlugin) {
		return this.bukkitInventory;
	}
	
	public static RequirementsInventory getInstance() {
		return instance;
	}
	
	@InTest
	@NeedsOptimization
    @Override
	public String handleChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin) {
		String properties = "{";
		
		boolean stop = false;
		boolean first = true;
		do{
			if(!first) {
                p.sendMessage(ChatColor.GREEN + "Click on a property type if you want to add another one");
            }
			String type = handleSingleTypeChoosing(p, editionPlayer, microPlugin, first);
			
			if(type == null || type.equals("null")) {
				stop = true;
				if(first) return null;
				return properties + "}";
			}else{
                p.sendMessage(ChatColor.GREEN + "Enter the name of the property:");
                String name = NextUtils.getNextMessageOf(p, true);

				properties+= (first ? "":",") + name + "|" + type;
			}
				
			first = false;
		}while(!stop);
		
		return properties;
	}
	
	private String handleSingleTypeChoosing(Player p, EditionPlayer editionPlayer, MicroPlugin microPlugin, boolean first) {
		p.openInventory(RequirementsInventory.getInstance().get(microPlugin));

		String s = "";
		
		ItemStack item = NextUtils.getNextClickedItemOf(p);
		if(!MicroPluginUtils.hasClickedARequirement(item)) {
			if(first) {
				p.sendMessage(ChatColor.RED + "You didn't click on any type! Stopping the current edition...");
			}
			return null;
		}
		
		
		String typeName = new StringBuilder(item.getItemMeta().getDisplayName()).delete(0, 5).toString();

        if(typeName.contains("Exit")) {
            return null;
        }

		LiquidRequirement type = LiquidRequirement.findByName(typeName);
		
		if(type == null) {
			p.sendMessage(ChatColor.RED + "Unknown type " + typeName);
			return null;
		}
		
		s+=typeName;
		
		return s;
	}

}
