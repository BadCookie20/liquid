package fr.badcookie20.liquid.core.events;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This custom event is fired when a reload occurs
 * @author BadCookie20
 * @since Beta 1.2
 */
public class CustomReloadEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
