package fr.badcookie20.liquid.core.events;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.condition.ConditionInstance;
import fr.badcookie20.liquid.core.listeners.*;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.utils.BukkitUtils;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class where every custom event must pass by
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidEvent {

    private static List<LiquidEvent> customEvents;
    
    static{
    	customEvents = new ArrayList<>();
    }

	private EventList e;
	private List<ActionInstance> actions;
    private MicroPlugin relatedMicroPlugin;
    private boolean cancel;
    private ConditionInstance cancelCondition;

    public LiquidEvent(EventList e, ConditionInstance cancelCondition, MicroPlugin relatedMicroPlugin) {
        this.e = e;
        this.actions = new ArrayList<>();
        this.relatedMicroPlugin = relatedMicroPlugin;

        e.getListener().addLiquidEvent(this);

        customEvents.add(this);

        this.cancel = true;
        this.cancelCondition = cancelCondition;
    }

    public LiquidEvent(EventList e, MicroPlugin relatedMicroPlugin) {
        this.e = e;
        this.actions = new ArrayList<>();
        this.relatedMicroPlugin = relatedMicroPlugin;

        e.getListener().addLiquidEvent(this);

        customEvents.add(this);

        this.cancel = true;
        this.cancelCondition = null;
    }

	public LiquidEvent(EventList e, List<ActionInstance> actions, MicroPlugin relatedMicroPlugin) {
		this.e = e;
		this.actions = actions;
        this.relatedMicroPlugin = relatedMicroPlugin;

        e.getListener().addLiquidEvent(this);
        
        customEvents.add(this);

        this.cancel = false;
        this.cancelCondition = null;
    }

	public EventList getHandledEvent() {
		return this.e;
	}

    public List<ActionInstance> getActions() {
        return this.actions;
    }
    
    public MicroPlugin getRelatedMicroPlugin() {
    	return this.relatedMicroPlugin;
    }
    
    public static List<LiquidEvent> getAllCustomEvents() {
    	return customEvents;
    }

    public boolean needsCancel() {
        return cancel;
    }

    public ConditionInstance getCancelCondition() {
        return this.cancelCondition;
    }

    /**
     * This enum is the enum containing all the events which are supported by the LiquidPlugin
     * @author BadCookie20
     * @since InDev 1.0
     */
    public enum EventList {

        PLAYER_JOIN_EVENT(PlayerJoinEvent.class, "PlayerJoinEvent", new PlayerJoinListener()),
        PLAYER_BREAK_BLOCK_EVENT(BlockBreakEvent.class, "PlayerBreakBlockEvent", new PlayerBreakBlockListener()),
        PLAYER_PLACE_BLOCK_EVENT(BlockPlaceEvent.class, "PlayerPlaceBlockEvent", new PlayerPlaceBlockListener()),
        PLAYER_DEATH_EVENT(PlayerDeathEvent.class, "PlayerDeathEvent", new PlayerDeathListener()),
        PLAYER_DROP_ITEM(PlayerDropItemEvent.class, "PlayerDropItemEvent", new PlayerDropItemListener()),
        PLAYER_BED_ENTER(PlayerBedEnterEvent.class, "PlayerBedEnterEvent", new PlayerBedEnterListener()),
        PLAYER_BED_LEAVE(PlayerBedLeaveEvent.class, "PlayerBedLeaveEvent", new PlayerBedLeaveListener()),
        RELOAD_EVENT(CustomReloadEvent.class, "ReloadEvent", new ReloadListener());


        private Class<? extends Event> eClass;
        private String name;
        private AbstractLiquidEventListener listener;

        EventList(Class<? extends Event> eClass, String name, AbstractLiquidEventListener listener) {
            this.eClass = eClass;
            this.name = name;
            this.listener = listener;

            BukkitUtils.registerListeners(listener);
        }

        public AbstractLiquidEventListener getListener() {
            return this.listener;
        }

        public Class<? extends Event> getEventClass() {
            return this.eClass;
        }

        public String getName() {
            return this.name;
        }

        public static List<Class<? extends Event>> getHandledEventsClasses() {
            List<Class<? extends Event>> events = new ArrayList<>();
            for(EventList e : EventList.values()) {
                events.add(e.eClass);
            }

            return events;
        }

        public static EventList getEventByClass(Class<? extends Event> clazz) {
            for(EventList r : EventList.values()) {
                if(r.eClass.equals(clazz)) {
                    return r;
                }
            }

            return null;
        }

    }
}
