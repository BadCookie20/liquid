package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is the exception that is thrown when an action generates an unexpected behaviour, or that the micro plugin file is corrupted when defining an action
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidActionException extends LiquidException {

	private static final long serialVersionUID = -3274043773302919433L;

	public LiquidActionException(String reason) {
		super(reason, true);
	}
	
}
