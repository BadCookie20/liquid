package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is the exception that is thrown when a command error happens
 * @author BadCookie20
 * @since InDev 1.1
 */
public class LiquidCommandException extends LiquidException {

	private static final long serialVersionUID = 3884953060739762885L;
	
	public LiquidCommandException(String reason) {
		super(reason, true);
	}

}
