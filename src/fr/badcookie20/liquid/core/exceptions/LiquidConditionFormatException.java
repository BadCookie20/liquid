package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is thrown when a condition cannot be checked because its objects are not compatible
 * @author BadCookie20
 * @since InDev 2.0
 */
public class LiquidConditionFormatException extends LiquidException {

	private static final long serialVersionUID = 8353848419314218276L;

	public LiquidConditionFormatException(String reason) {
		super(reason, false);
	}

}
