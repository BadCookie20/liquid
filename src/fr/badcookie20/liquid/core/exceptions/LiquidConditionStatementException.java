package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is the exception which is thrown when a condition statement is not well formated
 * @author BadCookie20
 * @since InDev 2.0
 */
public class LiquidConditionStatementException extends LiquidException {

	private static final long serialVersionUID = 1657309990097393765L;

	public LiquidConditionStatementException(String reason) {
		super(reason, false);
	}

}
