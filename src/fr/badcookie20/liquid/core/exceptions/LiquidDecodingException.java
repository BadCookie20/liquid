package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is the exception that is thrown when a decoding error happens when decoding a microplugin
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidDecodingException extends LiquidException {

	private static final long serialVersionUID = 1403590687602375721L;
	
	public LiquidDecodingException(String reason) {
		super(reason, false);
	}

}
