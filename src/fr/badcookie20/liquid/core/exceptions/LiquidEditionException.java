package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is thrown when an unexpected error happens while editing a micro-plugin
 * @author BadCookie20
 * @since InDev 3.0
 */
public class LiquidEditionException extends LiquidException {

    public LiquidEditionException(String reason) {
        super(reason, true);
    }
}

