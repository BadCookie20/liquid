package fr.badcookie20.liquid.core.exceptions;

import fr.badcookie20.liquid.core.LiquidPlugin;
import fr.badcookie20.liquid.core.utils.Logger;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the main class for all the LiquidExceptions. Every single LiquidException must extend this class
 * @author BadCookie20
 * @since InDev 1.1
 */
public abstract class LiquidException extends Exception {
	
	private static final long serialVersionUID = -3136191591761676342L;
	
	protected String reason;
	protected boolean internal;

    public LiquidException(String reason, boolean internal) {
        super(reason);
        this.reason = reason;
        this.internal = internal;
    }

    @Override
    public void printStackTrace() {
        if(LiquidPlugin.SHOW_STACK_TRACE) {
            super.printStackTrace();
        }else{
        	if(internal) {
        		Logger.logError("An internal error occured!");
        	}else{
        		Logger.logError("An error occured!");
        	}
            Logger.logError("Cause : " + this.reason);
            if(internal) {
            	Logger.logError(getInternalErrorDetailedMessage());
            }

			Logger.logError("If you think that is an error in the plugin and not in your micro-plugin, you may contact the developer (twitter: @badcookie20 and email: badcookie2000@gmail.com) to inform him about this error");
			Logger.logError("The actual version of the plugin is " + LiquidPlugin.VERSION);
        }
    }
    
    protected String getInternalErrorDetailedMessage() {
    	StackTraceElement[] stelements = this.getStackTrace();
    	StackTraceElement top = stelements[0];
    	StringBuilder showing = new StringBuilder();
    	showing.append("The exception occured in ");
    	showing.append(top.getClassName() + " class, while executing ");
    	showing.append(top.getMethodName() + " method, at line ");
    	showing.append(top.getLineNumber() + ".");
    	return showing.toString();
    }
}
