package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is the exception that is thrown when a IOException involved with the LiquidPlugin happens
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidIOException extends LiquidException {
	
	private static final long serialVersionUID = 2543771626398132981L;

	public LiquidIOException(String reason) {
        super(reason, true);
    }
}
