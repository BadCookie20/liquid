package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the exception that is thrown when an exception involved with parameters occurs.
 * @author BadCookie20
 * @since InDev 1.1
 */
public class LiquidParameterException extends LiquidException {
	
	private static final long serialVersionUID = -7407906658420482472L;
	
	public LiquidParameterException(String reason) {
		super(reason, true);
	}

}
