package fr.badcookie20.liquid.core.exceptions;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is thrown when an unexpected exception involved with reflection happens
 * @author BadCookie20
 * @since inDev 3.0
 */
public class LiquidReflectionException extends LiquidException {

	private static final long serialVersionUID = -8957686911242409486L;

	public LiquidReflectionException(String reason) {
        super(reason, false);
    }
}
