package fr.badcookie20.liquid.core.listeners;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.condition.ConditionInstance;
import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidReflectionException;
import fr.badcookie20.liquid.core.utils.ParameterUtils;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Listener;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This listener is the abstract listener for micro plugins events. Every listener for micro plugins must
 * extend this class
 * @author BadCookie20
 * @since InDev 1.0
 */
public abstract class AbstractLiquidEventListener implements Listener {

	private static List<AbstractLiquidEventListener> registered = new ArrayList<>();
	
    protected List<LiquidEvent> customEvents = new ArrayList<>();
    
    public AbstractLiquidEventListener() {
    	registered.add(this);
	}

    protected void execute(List<Object> possibleArguments, Cancellable event) {
        try {
            for (LiquidEvent ce : this.customEvents) {
                if(ce.needsCancel()) {
                    if(event == null) throw new LiquidReflectionException("could not cancel event " + ce.getHandledEvent().name() + " because it's not cancellable!");
                    if(ce.getCancelCondition() == null) {
                        event.setCancelled(true);
                        return;
                    }else {
                        ConditionInstance cancelCondition = ce.getCancelCondition();
                        ParameterUtils.fillInConditionMap(cancelCondition, possibleArguments);
                        if (cancelCondition.isTrue()) {
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
                for (ActionInstance a : ce.getActions()) {
                    a = ParameterUtils.fillInAction(a, possibleArguments);
                    a.execute();
                }
            }
        }catch(LiquidException | InvocationTargetException | IllegalAccessException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    protected void execute(List<Object> possibleArguments) {
    	try {
            for (LiquidEvent ce : this.customEvents) {
                if(ce.needsCancel()) throw new LiquidReflectionException("could not cancel event " + ce.getHandledEvent().name() + " because it's not cancellable!");
                for (ActionInstance a : ce.getActions()) {
                    a = ParameterUtils.fillInAction(a, possibleArguments);
                    a.execute();
                }
            }
        }catch(LiquidException | InvocationTargetException | IllegalAccessException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    public void addLiquidEvent(LiquidEvent e) {
        customEvents.add(e);
    }
    
    public static void clearEvents() {
    	for(AbstractLiquidEventListener e : registered) {
    		e.customEvents = new ArrayList<>();
    	}
    }

}
