package fr.badcookie20.liquid.core.listeners;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.SheepDyeWoolEvent;

import java.util.Arrays;
import java.util.List;

/**
 * This listener listens to the player dye sheep event
 * @author BadCookie20
 * @since Beta 1.2
 */
public class PlayerDyeSheepListener extends AbstractLiquidEventListener {

    @EventHandler
    public void onPlayerDyeSheep(SheepDyeWoolEvent e) {
        final List<Object> possibleArguments = Arrays.asList(e.getColor(), e.getEntity());
        this.execute(possibleArguments, e);
    }

}
