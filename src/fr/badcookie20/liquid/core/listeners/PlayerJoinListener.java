package fr.badcookie20.liquid.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This listener is the PlayerJoinListener for MicroPlugins
 * @author BadCookie20
 * @since InDev 1.0
 */
public class PlayerJoinListener extends AbstractLiquidEventListener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final List<Object> possibleArguments = Arrays.asList(e.getPlayer());
        this.execute(possibleArguments);
    }

}
