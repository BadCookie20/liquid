package fr.badcookie20.liquid.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This listener is the BlockPlaceEvent listener for MicroPlugins use
 * @author BadCookie20
 * @since InDev 2.0
 */
public class PlayerPlaceBlockListener extends AbstractLiquidEventListener {

	@EventHandler
    public void onPlayerPlaceBlock(BlockPlaceEvent e) {
        final List<Object> possibleArguments = Arrays.asList(e.getPlayer(), e.getBlock());
        this.execute(possibleArguments, e);
    }
	
}
