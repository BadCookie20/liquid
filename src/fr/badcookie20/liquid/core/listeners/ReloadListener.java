package fr.badcookie20.liquid.core.listeners;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.events.CustomReloadEvent;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;

/**
 * This listener listens to the reload event
 * @author BadCookie20
 * @since Beta 1.2
 */
public class ReloadListener extends AbstractLiquidEventListener {

    @EventHandler
    public void onReload(CustomReloadEvent e) {
        this.execute(new ArrayList<>());
    }

}
