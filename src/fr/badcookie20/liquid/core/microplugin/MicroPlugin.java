package fr.badcookie20.liquid.core.microplugin;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.commands.LiquidCommand;
import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.Logger;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class where every parameter of a MicroPlugin is stored
 * @author BadCookie20
 * @since InDev 1.0
 */
public class MicroPlugin {

	public static final String DEF_PREFIX = "def";
	public static final String EVENT_SUFFIX = "Event";
	public static final String CMD_PREFIX = "cmd";
    public static final String STRUCT_PREFIX = "struct";

    private static final String PREFIX = ChatColor.GREEN + ">";

	private boolean isDecoded;
	
	private List<LiquidEvent> events;
	private List<LiquidCommand> commands;
	private List<LiquidParameter> vars;
    private List<StructType> structs;

	private boolean showIndividualScoreboard = true;
    private boolean ignore = false;
    private boolean editOnlyOp = false;
    private boolean showDeclarations = true;

	private File f;
	private String name;

    private List<String> allLines;

    private List<String> about;
	
	public MicroPlugin(File f) {
        this.isDecoded = false;
        this.f = f;
        this.name = new StringBuilder(f.getName()).delete(f.getName().length() - 4, f.getName().length()).toString();
		this.vars = new ArrayList<>();
        this.commands = new ArrayList<>();
        this.events = new ArrayList<>();
        this.structs = new ArrayList<>();
		Logger.logInfo("Loaded successfully new microPlugin " + this.name);
	}
	
	public boolean isDecoded() {
		return this.isDecoded;
	}
	
	public List<LiquidEvent> getAllLiquidEvents() {
		return this.events;
	}
	
	public List<LiquidCommand> getAllLiquidCommands() {
		return this.commands;
	}
	
	public List<LiquidParameter> getAllVars() {
        return this.vars;
	}

    public List<StructType> getAllStructs() { return this.structs; }

    public void addVar(LiquidParameter var) {
        this.vars.add(var);
    }

    public boolean showScoreboard() {
        return this.showIndividualScoreboard;
    }

    public boolean isIgnored() { return this.ignore; }

    public boolean editOnlyOp() { return this.editOnlyOp; }

    public boolean showDeclarations() {
        return this.showDeclarations;
    }

    public String getName() {
        return this.name;
    }

    public void decode() throws LiquidException {
    	if(this.isDecoded) {
    		this.isDecoded = false;
    	}

        MicroPluginDecoder dec = new MicroPluginDecoder(this);
        this.allLines = dec.getAllLines();

        if(dec.isEmpty()) {
            Logger.logInfo(this.name + ">Ignored (empty micro-plugin)");
            return;
        }

        this.ignore = dec.getProperty("-ignore");
        this.showIndividualScoreboard = dec.getProperty("-showsc");
        this.editOnlyOp = dec.getProperty("-editop");
        this.showDeclarations = dec.getProperty("-showdec");

        if(this.ignore) {
            Logger.logInfo(this.name + ">Ignored (property -ignore=true)");
            this.isDecoded = false;
            return;
        }

        Logger.logInfo(this.name + ">Decoding...");

        this.structs = dec.getStructs();
        this.vars = dec.getVars();
        this.events = dec.getEvents();
        this.commands = dec.getCommands();

        this.isDecoded = true;
		dec.end();
        Logger.logInfo(this.name + ">Finished decoding !");

        initInfos();
    }

    public File getFile() {
        return f;
    }

    public void updateStruct(StructType inputStruct) {
        List<StructType> structs = new ArrayList<>();
        for(StructType struct : this.structs) {
            if(struct.getName().equals(inputStruct.getName())) {
                structs.add(inputStruct);
            }else{
                structs.add(struct);
            }
        }

        this.structs = structs;
    }

    public void updateVar(LiquidParameter inputVar) {
        List<LiquidParameter> vars = new ArrayList<>();
        for(LiquidParameter var : this.vars) {
            if(inputVar.getLiquidObject().getPath().equals(var.getLiquidObject().getPath())) {
                vars.add(inputVar);
            }else{
                vars.add(var);
            }
        }

        this.vars = vars;
    }

    public List<String> getAllLines() {
        return allLines;
    }

    public void sendInfos(Player p) {
        for(String line : about) {
            p.sendMessage(line);
        }
    }

    private void initInfos() {
        this.about = new ArrayList<>();

        about.add(ChatColor.GREEN + "---" + ChatColor.GOLD + "About " + getName() + ChatColor.GREEN + "---");
        about.add(PREFIX + ChatColor.AQUA + "File name: " + ChatColor.GOLD + getFile().getName());

        getHead("Properties", null);
        getDescription("-showdec", "value: " + showDeclarations(), false);
        getDescription("-editop", "value: " + editOnlyOp(), false);
        getDescription("-ignore", "value: " + isIgnored(), false);
        getDescription("-showsc", "value: " + showScoreboard(), false);

        if(!getAllVars().isEmpty()) {
            List<LiquidParameter> consts = getAllVars();
            getHead("Vars", consts);
            for(LiquidParameter param : consts) {
                String[] parts = LangUtils.split(param.getLiquidObject().getPath(), '_');
                getDescription(parts[2], "type: " + parts[1] + " constant:" + parts[0].equals("var"), false);
            }
        }else{
            getEmpty("No vars registered");
        }

        if(!getAllLiquidEvents().isEmpty()) {
            List<LiquidEvent> events = getAllLiquidEvents();
            getHead("Events listened", events);
            for(LiquidEvent event : events) {
                getDescription(event.getHandledEvent().getName(), event.getActions().size() + " action(s)", true);
                getActionsInfo(event.getActions());
            }
        }else{
            getEmpty("No events listened");
        }

        if(!getAllLiquidCommands().isEmpty()) {
            List<LiquidCommand> commands = getAllLiquidCommands();
            getHead("Commands", commands);
            for(LiquidCommand command : commands) {
                getDescription(command.getName(), command.getActions().size() + " action(s)", true);
                getActionsInfo(command.getActions());
            }
        }else{
            getEmpty("No commands registered");
        }

        if(!getAllStructs().isEmpty()) {
            List<StructType> structs = getAllStructs();
            getHead("Structs", structs);
            for(StructType s : structs) {
                getDescription(s.getName(), s.getProperties().size() + " properties", true);
            }
        }else{
            getEmpty("No structs registered");
        }

        about.add(ChatColor.GREEN + "---" + ChatColor.GOLD + "A LiquidMC micro-plugin" + ChatColor.GREEN + "---");
    }

    private void getActionsInfo(List<ActionInstance> actions) {
        if(actions.isEmpty()) return;

        for(ActionInstance action : actions) {
            String firstPart = ChatColor.GOLD + " |_" + action.getRelatedAction().toString();

            String instance = "";
            if(action.getRelatedAction().isStatic()) {
                instance = "no instance";
            }else{
                instance = "instance: " + action.getInstance().getLiquidObject().getPath();
            }

            String arguments = "";
            if(!action.getArguments().isEmpty()) {
                arguments = ChatColor.GRAY + "(parameters:";
                for(LiquidParameter param : action.getArguments()) {
                    arguments+=" " + param.getLiquidObject().getPath();
                }
                arguments+=")";
            }

            about.add(firstPart + " " + ChatColor.GRAY + instance + " " + ChatColor.GRAY + arguments);
        }
    }

    private void getDescription(String name, String details, boolean with) {
        about.add(ChatColor.GREEN + name + ChatColor.GRAY + " (" + (with ? "with ":"") + details + ")");
    }

    private void getHead(String title, List<?> list) {
        if(list == null) {
            about.add(PREFIX + ChatColor.AQUA + title + ":");
        }else{
            about.add(PREFIX + ChatColor.AQUA + title + ": " + ChatColor.GOLD + list.size());
        }
    }

    private void getEmpty(String message) {
        about.add(PREFIX + ChatColor.RED + message);
    }
}
