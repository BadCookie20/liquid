package fr.badcookie20.liquid.core.microplugin;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.actions.LiquidAction;
import fr.badcookie20.liquid.core.commands.LiquidCommand;
import fr.badcookie20.liquid.core.condition.ConditionInstance;
import fr.badcookie20.liquid.core.condition.ConditionType;
import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.exceptions.*;
import fr.badcookie20.liquid.core.microplugin.decoder.CommandsDecoder;
import fr.badcookie20.liquid.core.microplugin.decoder.EventsDecoder;
import fr.badcookie20.liquid.core.microplugin.decoder.StructsDecoder;
import fr.badcookie20.liquid.core.microplugin.decoder.VarsDecoder;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.ParameterUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the MicroPlugin decoder class. Use it to decode a MicroPluginFile (.lmp)
 * @author BadCookie20
 * @since InDev 1.0
 */
public class MicroPluginDecoder {

	private File f;
	private BufferedReader br;

	private List<String> lines;
    private List<String> allLines;

    private MicroPlugin microPlugin;
	
	public MicroPluginDecoder(MicroPlugin microPlugin) {
		this.f = microPlugin.getFile();
		this.microPlugin = microPlugin;
		getBufferedReader();
	    getLines();
	}
	
	private void getBufferedReader() {
        DataInputStream in = null;
        try {
            in = new DataInputStream(new FileInputStream(this.f));
        } catch (FileNotFoundException e) {
	    	 e.printStackTrace();
            return;
        }
        this.br = new BufferedReader(new InputStreamReader(in));
        this.lines = new ArrayList<>();
        this.allLines = new ArrayList<>();
	}
	
	private void getLines() {
		try {
			String readLine = "";
			while ((readLine = br.readLine()) != null) {
                this.allLines.add(readLine);
				if(readLine.startsWith("#") || readLine.isEmpty()) {
					continue;
				}else{
            		this.lines.add(readLine);
            	}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

    public boolean getProperty(String propertyName) throws LiquidDecodingException {
        if(MicroPluginProperty.getPropertyByName(propertyName) == null) throw new LiquidDecodingException("cannot resolve property " + propertyName);

        if(this.lines.isEmpty()) return false;
        for(String line : this.lines) {
            if(line.isEmpty()) continue;
            if(!line.startsWith(propertyName)) continue;
            return Boolean.parseBoolean(LangUtils.split(line, ':')[1]);
        }

        return MicroPluginProperty.getPropertyByName(propertyName).getDefaultValue();
    }
	
	public List<LiquidEvent> getEvents() throws LiquidException {
        return (List<LiquidEvent>) new EventsDecoder(this, this.microPlugin).get();
	}
	
	public List<LiquidCommand> getCommands() throws LiquidException {
        return (List<LiquidCommand>) new CommandsDecoder(this, this.microPlugin).get();
	}

    public List<StructType> getStructs() throws LiquidException {
        return (List<StructType>) new StructsDecoder(this, this.microPlugin).get();
    }

	public List<LiquidParameter> getVars() throws LiquidException {
        return (List<LiquidParameter>) new VarsDecoder(this, this.microPlugin).get();
	}

	public List<ActionInstance> getActionsInLine(String line, List<String> forbiddenStrings) throws LiquidException {
		List<ActionInstance> actions = new ArrayList<>();

		String[] splitted = LangUtils.splitIgnoreString(line, ' ');
		
		for(String s : splitted) {
            if(!s.equals(MicroPlugin.CMD_PREFIX) && !s.equals(MicroPlugin.DEF_PREFIX) && !s.contains(MicroPlugin.EVENT_SUFFIX) && !s.isEmpty() && !s.equals(" ") && !forbiddenStrings.contains(s)) {
                checkAction(s);
                String[] parts = LangUtils.splitIgnoreStringBracketsParentheses3parts(s);
                String actionName = parts[0];
                String parameters = parts[1];
                String[] params = LangUtils.splitIgnoreString(parameters, ',');

                String condition = null;
                String instance = null;

                List<LiquidParameter> liquidParams = new ArrayList<>();
                for (String paramString : params) {
                    liquidParams.add(ParameterUtils.fromStringToParam(paramString));
                }

                List<LiquidRequirement> requirements = new ArrayList<>();
                for (LiquidParameter param : liquidParams) {
                    boolean isVar = false;
                    for (LiquidParameter var : this.microPlugin.getAllVars()) {
                        String varName = LangUtils.split(var.getLiquidObject().getPath(), '_')[2];
                        if (varName.equals(param.getLiquidObject().getPath())) {
                            requirements.add(var.getLiquidObject().getRequirement());
                            liquidParams.remove(param);
                            liquidParams.add(var);
                            isVar = true;
                            break;
                        }
                    }

                    if (!isVar) {
                        requirements.add(param.getLiquidObject().getRequirement());
                    }
                }

                LiquidAction a = LiquidAction.findActionByName(actionName, requirements,
                        ((s.contains("->") && StringUtils.countMatches(s, "->") == 1 && !s.contains("get")) ||
                                (s.contains("->") && StringUtils.countMatches(s, "->") == 2)));

                if (a == null) {
                    throw new LiquidDecodingException("the computed action " + actionName + " is not registered! Check the spelling!");
                }

                if (isConditionPresent(s)) {
                    String afterBrackets = parts[2];
                    String[] afterBracketsParts = LangUtils.splitIgnoreStringBracketsCrochets(afterBrackets);
                    if (!a.isStatic()) {
                        instance = new StringBuilder(afterBracketsParts[0]).delete(0, 2).toString();
                    }
                    condition = afterBracketsParts[1];
                } else if (!a.isStatic()) {
                    instance = new StringBuilder(parts[2]).delete(0, 2).toString();
                }

                ActionInstance map = a.newActionMap();
                map.replaceArguments(liquidParams);

                if (!a.isStatic()) {
                    if (instance == null || instance.isEmpty()) {
                        throw new LiquidDecodingException("the action " + actionName + " needs an instance to work!");
                    }
                    map.replaceInstance(new LiquidParameter(instance));
                }

                if (condition != null) {
                    map.replaceCondition(getConditionMap(condition));
                }

                actions.add(map);
            }
        }
		
		return actions;
	}

    public void checkAction(String action) throws LiquidDecodingException {
        if(LangUtils.containsIgnoreString(action, ' ')) throw new LiquidDecodingException("the action " + action + " shouldn't contain a space");
        if(LangUtils.splitIgnoreStringBracketsParentheses3parts(action).length != 3) throw new LiquidDecodingException("there is a syntax error involving brackets in action " + action);
    }

    public ConditionInstance getConditionMap(String condition) throws LiquidDecodingException, LiquidParameterException, LiquidReflectionException, LiquidConditionStatementException {
        ConditionType type = ConditionType.getTypeByString(ParameterUtils.getConditionIndicator(condition));
        String[] conditionParts = ParameterUtils.toConditionParts(condition);
        LiquidParameter param1 = ParameterUtils.fromStringToParam(conditionParts[0]);
        LiquidParameter param2 = ParameterUtils.fromStringToParam(conditionParts[1]);
        return new ConditionInstance(type, param1, param2);
    }

    public boolean isConditionPresent(String line) {
        return line.contains("[") && line.contains("]");
	}

	public void end() {
        try {
            this.br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isEmpty() {
        return this.lines.isEmpty();
    }

    public List<String> getAllLines() {
        return allLines;
    }
}
