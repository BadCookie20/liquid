package fr.badcookie20.liquid.core.microplugin;

import fr.badcookie20.liquid.core.commands.CommandRegisterer;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.listeners.AbstractLiquidEventListener;
import fr.badcookie20.liquid.core.utils.Logger;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;

import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the MicroPluginManager. Every single MicroPlugin is registered and decoded here.
 * @author BadCookie20
 * @since InDev 1.0
 */
public class MicroPluginManager {

    private static MicroPluginManager instance;
    private List<MicroPlugin> microPlugins;

    private MicroPluginManager() {
        instance = this;

        this.microPlugins = new ArrayList<>();
        try {
            AbstractLiquidEventListener.clearEvents();
            CommandRegisterer.removeRegisteredCommands();
            this.microPlugins = MicroPluginUtils.getAllMicroPlugins();
        } catch (LiquidException e) {
            e.printStackTrace();
        }

        if(microPlugins.isEmpty()) {
            Logger.logWarning("There are no micro-plugins in the folder!");
            return;
        }

        for(MicroPlugin mp : microPlugins) {
            try {
                mp.decode();
            } catch (LiquidException e) {
                e.printStackTrace();
            }catch(ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Logger.logWarning(mp.getName() + ">Syntax error");
            }
        }
    }

    public List<MicroPlugin> getAllMicroPlugins() {
        return this.microPlugins;
    }
    
    public MicroPlugin getMicroPluginByName(String name) {
    	for(MicroPlugin microplugin : this.microPlugins) {
    		if(microplugin.getName().equals(name)) {
    			return microplugin;
    		}
    	}
    	
    	return null;
    }

    public static void init() {
        new MicroPluginManager();
    }

    public static MicroPluginManager getInstance() {
        return instance;
    }
}
