package fr.badcookie20.liquid.core.microplugin;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This enum stores all the possible properties
 * @author BadCookie20
 * @since Beta 1.3
 */
public enum MicroPluginProperty {

    SHOWSC("-showsc", true),
    SHOWDEC("-showdec", true),
    EDITOP("-editop", false),
    IGNORE("-ignore", false);

    private final boolean defaultValue;
    private String customName;

    MicroPluginProperty(String name, boolean defaultValue) {
        this.customName = name;
        this.defaultValue = defaultValue;
    }

    public static MicroPluginProperty getPropertyByName(String name) {
        for(MicroPluginProperty p : values()) {
            if(p.customName.equalsIgnoreCase(name)) {
                return p;
            }
        }

        return null;
    }

    public boolean getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String toString() {
        return this.customName;
    }

}
