package fr.badcookie20.liquid.core.microplugin.decoder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.commands.CommandRegisterer;
import fr.badcookie20.liquid.core.commands.LiquidCommand;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginDecoder;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class decodes the commands in the micro-plugins
 * @author BadCookie20
 * @since Release 1.0
 */
public class CommandsDecoder extends StatementDecoder {

    public CommandsDecoder(MicroPluginDecoder decoder, MicroPlugin microPlugin) {
        super(decoder, microPlugin);
    }

    @Override
    public List<?> get() throws LiquidException {
        this.log("Getting commands...");
        List<LiquidCommand> commands = new ArrayList<>();
        for(String line : this.lines) {
            if(!MicroPluginUtils.isDefLine(line)) {
                continue;
            }

            String removedDefDeclaration = MicroPluginUtils.removeDefDeclaration(line);
            if(!removedDefDeclaration.startsWith(MicroPlugin.CMD_PREFIX)) {
                continue;
            }

            String[] parts = LangUtils.split(removedDefDeclaration, ' ');
            String cmdName = parts[1];
            LiquidCommand cmd = CommandRegisterer.registerLiquidCommand(cmdName, this.decoder.getActionsInLine(line, Collections.singletonList(cmdName)), this.microPlugin);
            commands.add(cmd);
            this.log("Registered new command " + cmdName);
        }

        return commands;
    }
}
