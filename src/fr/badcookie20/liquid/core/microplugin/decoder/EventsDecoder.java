package fr.badcookie20.liquid.core.microplugin.decoder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.exceptions.LiquidDecodingException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginDecoder;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;
import org.bukkit.event.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * This class decodes the events in the micro-plugins
 * @author BadCookie20
 * @since Release 1.0
 */
public class EventsDecoder extends StatementDecoder {

    public EventsDecoder(MicroPluginDecoder decoder, MicroPlugin microPlugin) {
        super(decoder, microPlugin);
    }

    @Override
    public List<?> get() throws LiquidException {
        this.log("Getting events...");
        List<LiquidEvent> events = new ArrayList<>();
        for(String line : this.lines) {
            if(!line.split(" ")[0].contains(MicroPlugin.EVENT_SUFFIX ) || line.startsWith("#")) {
                continue;
            }

            Class<? extends Event> e = MicroPluginUtils.getEventInString(line);
            if(e == null) throw new LiquidDecodingException("the computed event " + line + " is not handled! Check the spelling");
            String[] parts = LangUtils.split(line, ' ');
            String action1 = parts[1];
            if(action1.contains("cancel")) {
                if(this.decoder.isConditionPresent(action1)) {
                    events.add(new LiquidEvent(LiquidEvent.EventList.getEventByClass(e), this.decoder.getConditionMap(LangUtils.splitIgnoreStringBracketsCrochets(action1)[1]), this.microPlugin));
                }else {
                    events.add(new LiquidEvent(LiquidEvent.EventList.getEventByClass(e), this.microPlugin));
                }
            }else {
                events.add(new LiquidEvent(LiquidEvent.EventList.getEventByClass(e), this.decoder.getActionsInLine(line, new ArrayList<>()), this.microPlugin));
            }

            this.log("Registered new event " + e.getSimpleName());
        }

        return events;
    }
}
