package fr.badcookie20.liquid.core.microplugin.decoder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginDecoder;
import fr.badcookie20.liquid.core.utils.Logger;

import java.util.List;

/**
 * This abstract class is the base class for all the decoders of LiquidMC
 * @author BadCookie20
 * @since Release 1.0
 */
public abstract class StatementDecoder {

    protected String name;
    protected boolean showDecs;
    protected List<String> lines;

    protected MicroPluginDecoder decoder;
    protected MicroPlugin microPlugin;

    public StatementDecoder(MicroPluginDecoder decoder, MicroPlugin microPlugin) {
        this.name = microPlugin.getName();
        this.showDecs = microPlugin.showDeclarations();
        this.lines = decoder.getAllLines();

        this.decoder = decoder;
        this.microPlugin = microPlugin;
    }

    public abstract List<?> get() throws LiquidException;

    protected void log(String message) {
        if(showDecs) Logger.logInfo(this.name + ">" + message);
    }

}
