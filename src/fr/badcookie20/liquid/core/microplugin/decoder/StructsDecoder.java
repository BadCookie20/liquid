package fr.badcookie20.liquid.core.microplugin.decoder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidDecodingException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginDecoder;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class decodes the structs in the micro-plugins
 * @author BadCookie20
 * @since Release 1.0
 */
public class StructsDecoder extends StatementDecoder {

    public StructsDecoder(MicroPluginDecoder decoder, MicroPlugin microPlugin) {
        super(decoder, microPlugin);
    }

    @Override
    public List<?> get() throws LiquidException {
        this.log("Getting structs...");
        List<StructType> structs = new ArrayList<>();
        for(String line : this.lines) {
            if(!MicroPluginUtils.isDefLine(line)) {
                continue;
            }

            String removedDefDeclaration = MicroPluginUtils.removeDefDeclaration(line);
            if(!removedDefDeclaration.startsWith(MicroPlugin.STRUCT_PREFIX)) {
                continue;
            }

            String[] parts = LangUtils.split(removedDefDeclaration, ' ');
            String structType = parts[1];
            String structName = parts[2];

            StructType.TypeList s = StructType.TypeList.getStructType(structType);
            StructType type = null;

            try {
                type = s.getStructClass().getConstructor(String.class, MicroPlugin.class).newInstance(structName, this.microPlugin);
            }catch(InstantiationException | IllegalAccessException | NullPointerException | NoSuchMethodException | InvocationTargetException e) {
                throw new LiquidDecodingException("couldn't instanciate structs " + structType);
            }

            String properties = new StringBuilder(parts[3]).deleteCharAt(0).deleteCharAt(parts[3].length() - 2).toString();
            String[] names = LangUtils.split(properties, ',');

            for(String property : names) {
                String[] parts2 = LangUtils.split(property, '|');
                type.addProperty(parts2[0], LiquidRequirement.findByName(parts2[1]));
            }

            structs.add(type);
            this.log("Registered new struct " + name);
        }

        return structs;
    }
}
