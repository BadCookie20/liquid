package fr.badcookie20.liquid.core.microplugin.decoder;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.consts.VarType;
import fr.badcookie20.liquid.core.exceptions.LiquidDecodingException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginDecoder;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.MicroPluginUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class decodes the constants in the micro-plugins
 * @author BadCookie20
 * @since Release 1.0
 */
public class VarsDecoder extends StatementDecoder {

    public VarsDecoder(MicroPluginDecoder decoder, MicroPlugin microPlugin) {
        super(decoder, microPlugin);
    }

    @Override
    public List<?> get() throws LiquidException {
        this.log("Getting vars...");
        List<LiquidParameter> vars = new ArrayList<>();
        for(String line : this.lines) {
            if(!MicroPluginUtils.isDefLine(line)) {
                continue;
            }

            String removedDefDeclaration = MicroPluginUtils.removeDefDeclaration(line);
            if(!VarType.startWithVarType(removedDefDeclaration)) {
                continue;
            }

            String[] parts = LangUtils.splitIgnoreString(removedDefDeclaration, ' ');

            boolean isConstant = false;
            if(parts.length > 2) isConstant = true;

            String type = parts[0];
            String varName = parts[1];

            String value;
            String[] valueParts = new String[0];

            if(isConstant) {
                if (!parts[2].equals("=")) {
                    throw new LiquidDecodingException("cannot define var when assignment equals " + parts[2] + "!");
                }

                value = parts[3];
                valueParts = LangUtils.splitIgnoreString(value, ',');
            }

            VarType varType = VarType.getVarType(type);
            if(varType == null) throw new LiquidParameterException("unknown var type " + type);
            LiquidParameter var;

            if(isConstant) {
                var = varType.buildConstant(Arrays.asList(valueParts), varName);
            }else{
                var = varType.buildVar(Arrays.asList(valueParts), varName);
            }

            vars.add(var);
            this.log("Added new var " + varName);
        }

        return vars;
    }
}
