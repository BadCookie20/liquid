package fr.badcookie20.liquid.core.reflect;

import fr.badcookie20.liquid.core.consts.VarType;
import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.reflect.enums.AbstractLiquidEnum;
import fr.badcookie20.liquid.core.reflect.types.LiquidType;
import fr.badcookie20.liquid.core.reflect.types.TypeBlock;
import fr.badcookie20.liquid.core.reflect.types.TypePlayer;
import fr.badcookie20.liquid.core.structs.StructType;
import fr.badcookie20.liquid.core.utils.LangUtils;
import fr.badcookie20.liquid.core.utils.ParameterUtils;
import org.apache.commons.lang.math.NumberUtils;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class which can store an object path, and return its requirement. It may
 * be used with LiquidParameter
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidObject {

	private String path;
    private LiquidRequirement preRequirement;
    private LiquidType preType;
	
	public LiquidObject(String path) {
		this.path = path;
	}

    public LiquidObject(String path, LiquidRequirement preRequirement) {
        this.path = path;
        this.preRequirement = preRequirement;
        if(this.preRequirement == LiquidRequirement.REQUIRES_PLAYER) this.preType = new TypePlayer();
        if(this.preRequirement == LiquidRequirement.REQUIRES_BLOCK) this.preType = new TypeBlock();
    }
	
    public LiquidRequirement getRequirement() throws LiquidParameterException {
        if(ParameterUtils.isGetter(this.path)) {
            return preRequirement;
        }

        if(ParameterUtils.isBoolean(this.path)) {
            return LiquidRequirement.REQUIRES_BOOLEAN;
        }

        if(path.startsWith("\"") && path.endsWith("\"")) {
            return LiquidRequirement.REQUIRES_STRING;
        }

        if(ParameterUtils.isEnumValue(this.path)) {
            return AbstractLiquidEnum.EnumList.getRequirementOfFullPath(this.path);
        }
        
        if(ParameterUtils.isBoolean(this.path)) {
        	return LiquidRequirement.REQUIRES_BOOLEAN;
        }

        if(NumberUtils.isNumber(path)) {
            return LiquidRequirement.REQUIRES_NUMBER;
        }

        System.out.println("this.path: " + this.path);

        if(this.getPath().contains("_")) {
            String type = LangUtils.split(this.getPath(), '_')[1];
            return VarType.getVarType(type).getType();
        }

        for(MicroPlugin mp : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            for(StructType structType : mp.getAllStructs()) {
                if(structType.getName().equalsIgnoreCase(this.path)) {
                    return LiquidRequirement.REQUIRES_STRUCT;
                }
            }
        }

		if(!path.contains(".")) {
            if(LiquidType.TypeList.get(path) == null) throw new LiquidParameterException("unknown type " + this.path);
            LiquidRequirement requirement = LiquidType.TypeList.get(path).getRequirementOfSuperType();
            if(requirement == null) throw new LiquidParameterException("could not get requirement of " + this.path);
            return requirement;
        }
        String[] parts = LangUtils.split(path, '.');

        LiquidType superType = LiquidType.TypeList.get(parts[0]);

        return superType.getRequirementOfSubtype(parts[1]);
	}

    public LiquidType getSuperType() {
        LiquidType p;

        if(ParameterUtils.isGetter(this.path)) {
            return this.preType;
        }

        if(!path.contains(".")) {
            p =  LiquidType.TypeList.get(path);
        }else{
            p =  LiquidType.TypeList.get(LangUtils.split(this.path, '.')[0]);
        }

        return p;
    }

    public boolean hasSubType() {
        return this.path.contains(".");

    }

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
