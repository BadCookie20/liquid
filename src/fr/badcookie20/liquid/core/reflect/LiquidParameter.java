package fr.badcookie20.liquid.core.reflect;

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents a parameter that may be used for an action purpose, after having been read by
 * the decoder.
 * @author BadCookie20
 * @since InDev 1.0
 */
public class LiquidParameter {

    private LiquidObject type;
	private Object content;
    private boolean unereasable;

    public LiquidParameter(String path) {
        System.out.println("Calling 1 for path " + path);
        this.type = new LiquidObject(path);
        this.unereasable = false;
    }

    public LiquidParameter(String path, LiquidRequirement preRequirement) {
        System.out.println("Calling 2 for path " + path);
        this.type = new LiquidObject(path, preRequirement);
        this.unereasable = false;
    }

    public void setUnereasable(boolean unereasable) {
        this.unereasable = unereasable;
    }

    public boolean isUnereasable() {
        return this.unereasable;
    }

    public LiquidObject getLiquidObject() {
        return type;
    }

    public void setContent(Object content) throws LiquidParameterException {
        if(content == null) {
            if(this.unereasable) {
                throw new LiquidParameterException("cannot erease the content of a unereasable parameter!");
            }
            this.content = content;
            return;
        }

        /*if(!content.getClass().isAssignableFrom(type.getRequirement().getClassType()) && !type.getRequirement().getClassType().isAssignableFrom(content.getClass())) {
            throw new LiquidParameterException("the type specified while setting the content does not match the parameter type!");
        }*/
        this.content = content;
    }
	
	public Object getContent() {
		return this.content;
	}
	
}

