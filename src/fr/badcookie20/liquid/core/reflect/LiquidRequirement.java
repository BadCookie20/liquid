package fr.badcookie20.liquid.core.reflect;

import fr.badcookie20.liquid.core.structs.StructType;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the class where every requirement must be registered
 * @author BadCookie20
 * @since InDev 1.0
 */
public enum LiquidRequirement {

	REQUIRES_LOCATION(Location.class, "location"),
	REQUIRES_BOOLEAN(Boolean.class, "boolean"),
	REQUIRES_PLAYER(Player.class, "player"),
	REQUIRES_WORLD(World.class, "world"),
	REQUIRES_STRING(String.class, "string"),
	REQUIRES_MATERIAL(Material.class, "type"),
	REQUIRES_BLOCK(Block.class, "block"),
	REQUIRES_NUMBER(Number.class, "number"),
	REQUIRES_ITEMSTACK(ItemStack.class, "item"),
    REQUIRES_INVENTORY(PlayerInventory.class, "inventory"),
	REQUIRES_GAMEMODE(GameMode.class, "gamemode"),
	REQUIRES_STRUCT(StructType.class, "struct"),
    REQUIRES_OBJECT(Object.class, "object"),
	NULL(null, "null");

    private final String displayName;
    private Class<?> requirement;
	
	LiquidRequirement(Class<?> requirement, String displayName) {
		this.requirement = requirement;
		this.displayName = displayName;
	}

	public Class<?> getClassType() {
		return this.requirement;
	}
	
	public static LiquidRequirement getRequirementByClass(Class<?> clazz) {
        for(LiquidRequirement r : values()) {
            if(r.requirement.isAssignableFrom(clazz)) {
                return r;
			}
		}
		
		return null;
	}

	public static LiquidRequirement findByName(String s) {
        for(LiquidRequirement liquidRequirement : values()) {
            if(liquidRequirement.displayName.equalsIgnoreCase(s)) {
                return liquidRequirement;
            }
        }

        return null;
	}
	
	@Override
	public String toString() {
		return this.displayName;
	}
}
