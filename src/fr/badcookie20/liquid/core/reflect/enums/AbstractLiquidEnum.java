package fr.badcookie20.liquid.core.reflect.enums;

import fr.badcookie20.liquid.core.exceptions.LiquidReflectionException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.LangUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the base class for all the enums handled
 * @author BadCookie20
 * @since InDev 3.0
 */
public abstract class AbstractLiquidEnum {

    private final LiquidRequirement requirement;
    private Class<? extends Enum> enumBase;
    private String displayName;

    public AbstractLiquidEnum(Class<?> enumBase, String displayName, LiquidRequirement requirement) {
        if(enumBase.isEnum()) {
            this.enumBase = (Class<? extends Enum>) enumBase;
        }
        this.displayName = displayName;
        this.requirement = requirement;
    }

    public Object getElement(String subType) throws IllegalAccessException {
        for(Field f : enumBase.getDeclaredFields()) {
            if(f.getName().equals(subType)) {
                return f.get(null);
            }
        }

        return null;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public LiquidRequirement getRequirement() {
        return this.requirement;
    }

    /**
     * This enum stores all the Bukkit enums handled by the plugin
     * @author BadCookie20
     * @since InDev 3.0
     */
    public enum EnumList {

        MATERIAL(new LiquidMaterialEnum()),
        GAMEMODE(new LiquidGameModeEnum());

        private AbstractLiquidEnum liquidEnum;

        EnumList(AbstractLiquidEnum liquidEnum) {
            this.liquidEnum = liquidEnum;
        }

        public static List<AbstractLiquidEnum> getAllLiquidEnums() {
            List<AbstractLiquidEnum> elements = new ArrayList<>();

            for(EnumList e : EnumList.values()) {
                elements.add(e.liquidEnum);
            }

            return elements;
        }

        public static LiquidRequirement getRequirementOfFullPath(String fullName) {
            return getEnumByFullName(fullName).getRequirement();
        }

        public static Object getEnumValue(String fullName) throws LiquidReflectionException {
            if(!fullName.contains(".")) {
                throw new LiquidReflectionException("cannot return the value of an enum when the path (" + fullName + ") does not contain object target!");
            }

            Object value;

            try {
                value = getEnumByFullName(fullName).getElement(LangUtils.split(fullName, '.')[1]);
            } catch (IllegalAccessException e) {
                throw new LiquidReflectionException("cannot access the enum targeted with the path " + fullName + "! Check the spelling!");
            }

            return value;
        }

        public static AbstractLiquidEnum getEnumByFullName(String fullName) {
            if(fullName.contains(".")) {
                return getEnum(LangUtils.split(fullName, '.')[0]);
            }else{
                return getEnum(fullName);
            }
        }

        public static AbstractLiquidEnum getEnum(String fullName) {
            for(AbstractLiquidEnum e : getAllLiquidEnums()) {
                if(e.getDisplayName().equals(fullName)) {
                    return e;
                }
            }

            return null;
        }
    }
}
