package fr.badcookie20.liquid.core.reflect.enums;

import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import org.bukkit.GameMode;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the {@link GameMode} enum
 * @author BadCookie20
 * @since InDev 3.0
 */
public class LiquidGameModeEnum extends AbstractLiquidEnum {

    public LiquidGameModeEnum() {
        super(GameMode.class, "GAMEMODE", LiquidRequirement.REQUIRES_GAMEMODE);
    }
}
