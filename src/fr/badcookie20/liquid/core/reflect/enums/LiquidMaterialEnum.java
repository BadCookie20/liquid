package fr.badcookie20.liquid.core.reflect.enums;

import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import org.bukkit.Material;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the {@link Material} enum
 * @author BadCookie20
 * @since InDev 3.0
 */
public class LiquidMaterialEnum extends AbstractLiquidEnum {

    public LiquidMaterialEnum() {
        super(Material.class, "TYPE", LiquidRequirement.REQUIRES_MATERIAL);
    }
}
