package fr.badcookie20.liquid.core.reflect.types;

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.LangUtils;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the super type of all the reflection elements in Liquid
 * @author BadCookie20
 * @since InDev 1.0
 */
public abstract class LiquidType {
	
	private LiquidRequirement requirement;
	private String name;
    protected java.util.List assignableRequirements;
	
	public LiquidType(LiquidRequirement requirement, String name, java.util.List assignableRequirements) {
		this.requirement = requirement;
		this.name = name;
        this.assignableRequirements = assignableRequirements;
	}

    public Object get(String fullPath, Object o) throws LiquidParameterException {
        if(!fullPath.contains(".")) {
            return o;
        }else{
            return getSubtype(LangUtils.split(fullPath, '.')[1], o);
        }
    }

    public String getName() {
        return this.name;
    }
    
    public Class<?> getClassType() {
    	return this.requirement.getClassType();
    }

	public LiquidRequirement getRequirementOfSuperType() {
		return this.requirement;
	}

    public abstract LiquidRequirement getRequirementOfSubtype(String subType);

    public abstract Object getSubtype(String subType, Object o) throws LiquidParameterException;

    public java.util.List getAssignableRequirements() {
        return this.assignableRequirements;
    }

    /**
     * This enum stores all the LiquidReflection types
     * @author BadCookie20
     * @since InDev 1.0
     */
    public enum TypeList {

        PLAYER(new TypePlayer()),
        BLOCK(new TypeBlock());

        private LiquidType type;

        TypeList(LiquidType type) {
            this.type = type;
        }


        public static LiquidType get(String string) {
            for(TypeList t : TypeList.values()) {
                LiquidType type = t.type;
                if(type.getName().equalsIgnoreCase(string)) {
                    return type;
                }
            }

            return null;
        }

        public static LiquidType get(Class<?> clazz) {
             for(TypeList t : TypeList.values()) {
                    LiquidType type = t.type;
                    if(type.getClassType().equals(clazz)) {
                        return type;
                    }
                }
                return null;
        }

    }
}
