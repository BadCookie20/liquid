package fr.badcookie20.liquid.core.reflect.types;

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the {@link Block} type
 * @author BadCookie20
 * @since InDev 2.0
 * @see Block
 */
public final class TypeBlock extends LiquidType {

	public TypeBlock() {
		super(LiquidRequirement.REQUIRES_BLOCK, "block", Arrays.asList(LiquidRequirement.REQUIRES_LOCATION,
				LiquidRequirement.REQUIRES_STRING,
				LiquidRequirement.REQUIRES_MATERIAL,
				LiquidRequirement.REQUIRES_ITEMSTACK,
				LiquidRequirement.REQUIRES_NUMBER,
				LiquidRequirement.REQUIRES_WORLD));
	}

	@Override
	public LiquidRequirement getRequirementOfSubtype(String subType) {
		switch(subType) {
	    case "type":
		    return LiquidRequirement.REQUIRES_MATERIAL;
        case "item":
            return LiquidRequirement.REQUIRES_ITEMSTACK;
	    case "typename":
	    	return LiquidRequirement.REQUIRES_STRING;
        case "typeid":
            return LiquidRequirement.REQUIRES_NUMBER;
	    case "loc":
	    	return LiquidRequirement.REQUIRES_LOCATION;
	    case "world":
	    	return LiquidRequirement.REQUIRES_WORLD;
		}
	
		return null;
	}

	@Override
	public Object getSubtype(String subType, Object o) throws LiquidParameterException {
		if (!(o instanceof Block)) {
			throw new LiquidParameterException("cannot return value of subtype, because the object specified is not the type expected!");
		}

		Block b = (Block) o;

		switch (subType) {
			case "type":
				return b.getType();
			case "item":
				return new ItemStack(b.getType());
			case "typename":
				return b.getType().toString();
			case "typeid":
				return b.getType().getId();
			case "loc":
				return b.getLocation();
			case "world":
				return b.getWorld();
		}

		return null;
	}
}
