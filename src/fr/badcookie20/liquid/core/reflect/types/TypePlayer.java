package fr.badcookie20.liquid.core.reflect.types;

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import org.bukkit.entity.Player;

import java.util.Arrays;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class represents the {@link Player} type
 * @author BadCookie20
 * @since InDev 1.0
 * @see Player
 */
public final class TypePlayer extends LiquidType {

	public TypePlayer() {
		super(LiquidRequirement.REQUIRES_PLAYER, "player",
				Arrays.asList(LiquidRequirement.REQUIRES_LOCATION,
                        LiquidRequirement.REQUIRES_STRING,
                        LiquidRequirement.REQUIRES_INVENTORY,
                        LiquidRequirement.REQUIRES_WORLD,
                        LiquidRequirement.REQUIRES_NUMBER,
                        LiquidRequirement.REQUIRES_BOOLEAN,
                        LiquidRequirement.REQUIRES_GAMEMODE));
	}

	@Override
	public LiquidRequirement getRequirementOfSubtype(String subType) {
        switch(subType) {
		    case "loc":
		    case "headloc":
		    	return LiquidRequirement.REQUIRES_LOCATION;
		    case "name":
			    return LiquidRequirement.REQUIRES_STRING;
            case "world":
                return LiquidRequirement.REQUIRES_WORLD;
            case "inv":
                return LiquidRequirement.REQUIRES_INVENTORY;
            case "gamemode":
            	return LiquidRequirement.REQUIRES_GAMEMODE;
            case "health":
                return LiquidRequirement.REQUIRES_NUMBER;
            case "isOp":
                return LiquidRequirement.REQUIRES_BOOLEAN;
		}
		
		return null;
	}

    @Override
    public Object getSubtype(String subType, Object o) throws LiquidParameterException {
        if(!(o instanceof Player)) {
            throw new LiquidParameterException("cannot return value of subtype, because the object specified is not the type expected!");
        }

        Player p = (Player) o;

        switch(subType) {
            case "loc":
                return p.getLocation();
            case "headloc":
            	return p.getEyeLocation();
            case "name":
                return p.getDisplayName();
            case "world":
                return p.getLocation().getWorld();
            case "inv":
                return p.getInventory();
            case "gamemode":
            	return p.getGameMode();
            case "health":
                return p.getHealth();
            case "isOp":
                return p.isOp();

        }
        return null;
    }


}
