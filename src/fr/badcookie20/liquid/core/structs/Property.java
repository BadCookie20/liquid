package fr.badcookie20.liquid.core.structs;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;

import java.util.HashMap;

/**
 * This class is represents each property of a structure
 * @author BadCookie20
 * @since Beta 1.2
 */
public class Property {

    private String name;
    private LiquidRequirement type;
    private HashMap<Object,Object> value;

    public Property(String propertyName, LiquidRequirement type) throws LiquidParameterException {
        this.name = propertyName;
        if(type == null) {
            throw new LiquidParameterException("unknown requirement of property " + propertyName);
        }
        this.type = type;
        this.value = new HashMap<>();
    }

    public void setValue(Object key, Object value) {
        if(this.value.containsKey(key)) {
            this.value.replace(key, value);
        }else{
            this.value.put(key, value);
        }
    }

    public Object getValue(Object key) {
        return this.value.get(key);
    }

    public LiquidRequirement getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
