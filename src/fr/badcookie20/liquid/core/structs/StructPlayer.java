package fr.badcookie20.liquid.core.structs;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import org.bukkit.Bukkit;

import java.util.HashMap;

/**
 * This class is the class which inherits the player structure
 * @author BadCookie20
 * @since Beta 1.2
 */
public class StructPlayer extends StructType {

    public StructPlayer(String name, MicroPlugin microPlugin) {
        super(name, new HashMap<>(), microPlugin);
    }

    @Override
    protected Object getKey(String key) {
        return Bukkit.getPlayer(key).getName();
    }

}
