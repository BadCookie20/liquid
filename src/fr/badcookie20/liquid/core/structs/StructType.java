package fr.badcookie20.liquid.core.structs;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.exceptions.LiquidReflectionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.utils.LangUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class is the base class for all structures.
 * @author BadCookie20
 * @since Beta 1.2
 */
public abstract class StructType {

    protected List<Property> properties;
    protected String name;
    protected MicroPlugin microPlugin;

    public StructType(String name, HashMap<String, LiquidRequirement> properties, MicroPlugin microPlugin) {
        this.properties = new ArrayList<>();
        try {
            for (String propertyName : properties.keySet()) {
                this.properties.add(new Property(propertyName, properties.get(propertyName)));
            }
        }catch(LiquidParameterException e) {
            e.printStackTrace();
        }
        this.name = name;
        this.microPlugin = microPlugin;
    }

    public void addProperty(String propertyName, LiquidRequirement type) {
        try {
            this.properties.add(new Property(propertyName, type));
        } catch (LiquidParameterException e) {
            e.printStackTrace();
        }
    }

    public void addProperties(HashMap<String, LiquidRequirement> properties) {
        for(String propertyName : properties.keySet()) {
            this.addProperty(propertyName, properties.get(propertyName));
        }
    }

    public void setProperty(String property, Object value) {
        String key = LangUtils.split(property, '/')[0];
        String propertyName = LangUtils.split(property, '/')[1];

        for(Property propr : properties) {
            if (propr.getName().equals(propertyName)) {
                this.properties.remove(propr);
                propr.setValue(getKey(key), value);
                this.properties.add(propr);
                break;
            }
        }

        this.microPlugin.updateStruct(this);
    }

    public LiquidRequirement getTypeOfProperty(String propertyName) {
        if(propertyName.contains("/")) {
            return getTypeOfProperty(LangUtils.split(propertyName, '/')[1]);
        }
        for(Property propr : properties) {
            if (propr.getName().equals(propertyName)) {
                return propr.getType();
            }
        }

        return null;
    }

    public Object getProperty(String property) throws LiquidReflectionException {
        String key = LangUtils.split(property, '/')[0];
        String propertyName = LangUtils.split(property, '/')[1];

        Object result = null;
        boolean found = false;

        for(Property propr : properties) {
            if(propr.getName().equals(propertyName)) {
                found = true;
                result = propr.getValue(key);
            }
        }

        if(!found) throw new LiquidReflectionException("cannot access " + propertyName + " property of structs " + this.name + " because it doesn't exist!");
        if(result == null) throw new LiquidReflectionException("the value of " + propertyName + " property for key " + key + " of structs " + this.name + " is null. It may not have been initialized");

        return result;
    }

    public MicroPlugin getRelatedMicroPlugin() {return this.microPlugin; }

    public String getName() {
        return this.name;
    }

    protected abstract Object getKey(String key);

    public List<Property> getProperties() {
        return properties;
    }

    /**
     * This enum stores all the structs types available
     * @author BadCookie20
     * @since Beta 1.2
     */
    public enum TypeList {

        PLAYER(StructPlayer.class, "player");

        private Class<? extends StructType> clazz;
        private String name;

        TypeList(Class<? extends StructType> clazz, String name) {
            this.clazz = clazz;
            this.name = name;
        }

        public Class<? extends StructType> getStructClass() {
            return this.clazz;
        }

        public static TypeList getStructType(String name) {
            for(TypeList s : TypeList.values()) {
                if(s.name.equalsIgnoreCase(name)) {
                    return s;
                }
            }

            return null;
        }

    }
}
