    package fr.badcookie20.liquid.core.utils;

import fr.badcookie20.liquid.core.LiquidPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.spigotmc.AsyncCatcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This utils class contains useful methods involved with bukkit
 * @author BadCookie20
 * @since InDev 1.0
 */
public class BukkitUtils {

	private static List<Listener> registeredListeners = new ArrayList<>();

	public static void registerListeners(Listener... listeners) {
        for(Listener l : listeners) {
			if(registeredListeners.contains(l)) {
				continue;
			}
			Bukkit.getPluginManager().registerEvents(l, LiquidPlugin.getInstance());
		}

		registeredListeners.addAll(Arrays.asList(listeners));
	}
	
	public static void disableAsyncCatcher() {
        try {
            if (AsyncCatcher.enabled) AsyncCatcher.enabled = false;
        }catch(NoClassDefFoundError e) {
            Logger.logWarning("The AsyncCatcher class couldn't be found! Bugs may happen");
        }
	}
	
	public static void enableAsyncCatcher() {
        try {
            if (AsyncCatcher.enabled) AsyncCatcher.enabled = true;
        }catch(NoClassDefFoundError e) {
            Logger.logWarning("The AsyncCatcher class couldn't be found! Bugs may happen");
        }
	}

    public static boolean isCancellable(Event event) {
        List<Class<?>> interfaces = Arrays.asList(event.getClass().getInterfaces());
        return interfaces.contains(Cancellable.class);
    }

	public static Inventory renameInventory(Inventory inv, String newName) {
        int size = inv.getSize();

        List<ItemStack> fullContents = new ArrayList<>();

        for(int i = 0; i < size; i++) {
            ItemStack item = inv.getItem(i);
            if(item == null || item.getType() == Material.AIR) {
                fullContents.add(new ItemStack(Material.AIR));
            }else{
                fullContents.add(item);
            }
        }

        Inventory newInventory = Bukkit.createInventory(null, size, newName);
        newInventory.setContents(fullContents.toArray(new ItemStack[fullContents.size()]));

        return newInventory;
    }

    public static ItemStack createItemStack(Material material, String name, List<String> lores) {
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        if(name != null) itemMeta.setDisplayName(name);
        if(lores != null && !lores.isEmpty()) itemMeta.setLore(lores);
        item.setItemMeta(itemMeta);
        return item;
    }

    public static String placeColor(String s) {
        if(!s.contains("&")) return s;

        return s.replace("&", "§");
    }
}
