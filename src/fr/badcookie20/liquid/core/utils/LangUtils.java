package fr.badcookie20.liquid.core.utils;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
/**
 * This util class contains miscellaneous utils methods
 * @author BadCookie20
 * @since InDev 1.2
 */
public class LangUtils {

    public static boolean containsIgnoreString(String s, char regex) {
        boolean inString = false;
        for(char c : s.toCharArray()) {
            if(c == '"') inString = !inString;
            if(c == regex && !inString) return true;
        }

        return false;
    }

	public static String[] split(String s, char regex) {
	    List<String> splitted = new ArrayList<>();
	    StringBuilder tempString = new StringBuilder();
	    for(char c : s.toCharArray()) {
	        if(c != regex) {
	            tempString.append(c);
	        }else{
	            splitted.add(tempString.toString());
	            tempString = new StringBuilder();
	        }
	    }

	    splitted.add(tempString.toString());
	
	    String[] splittedArray = new String[splitted.size()];
	    int i = 0;
	    for(String temp : splitted) {
	        splittedArray[i] = temp;
	        i++;
	    }
	
	    return splittedArray;
	}

	public static String[] splitIgnoreString(String s, char regex) {
		List<String> splitted = new ArrayList<>();
	    StringBuilder tempString = new StringBuilder();
	    boolean inString = false;
	    for(char c : s.toCharArray()) {
	    	if(c == '"') {
                inString = !inString;
	    	}
	    	
	        if(c != regex) {
	            tempString.append(c);
	        }else{
	        	if(!inString) {
                    splitted.add(tempString.toString());
		            tempString = new StringBuilder();
	        	}else{
                    tempString.append(c);
                }
	        }
	    }
	
	    splitted.add(tempString.toString());
	
	    String[] splittedArray = new String[splitted.size()];
	    int i = 0;
	    for(String temp : splitted) {
	        splittedArray[i] = temp;
	        i++;
	    }
	
	    return splittedArray;
	}

	public static String[] splitIgnoreStringBracketsParentheses(String s) {
        List<String> splitted = new ArrayList<>();
        StringBuilder tempString = new StringBuilder();
        boolean inString = false;
        for(char c : s.toCharArray()) {
            if(c == '"') {
                inString = !inString;
            }

            if(c != '(' && c != ')') {
                tempString.append(c);
            }else{
                if(!inString) {
                    splitted.add(tempString.toString());
                    tempString = new StringBuilder();
                }
            }
        }

        splitted.add(tempString.toString());

        String[] splittedArray = new String[splitted.size()];
        int i = 0;
        for(String temp : splitted) {
            splittedArray[i] = temp;
            i++;
        }

        return splittedArray;
    }

    public static String[] splitIgnoreStringBracketsParentheses3parts(String s) {
        List<String> splitted = new ArrayList<>();
        StringBuilder tempString = new StringBuilder();
        boolean inString = false;
        boolean opened = false;
        boolean closed = false;

        int closingBrackets = StringUtils.countMatches(s, ")");
        int j = 1;

        for(char c : s.toCharArray()) {
            if(c == '"') {
                inString = !inString;
            }

            if(c != '(' && c != ')') {
                tempString.append(c);
            }else{
                if(!inString) {
                    if(c == '(' && !opened) {
                        opened = true;
                        splitted.add(tempString.toString());
                        tempString = new StringBuilder();
                    }else if(c == ')' && !closed) {
                        if(closingBrackets == j) {
                            closed = true;
                            splitted.add(tempString.toString());
                            tempString = new StringBuilder();
                        }else{
                            j++;
                            tempString.append(c);
                        }
                    }else{
                        tempString.append(c);
                    }
                }
            }
        }

        splitted.add(tempString.toString());

        String[] splittedArray = new String[splitted.size()];
        int i = 0;
        for(String temp : splitted) {
            splittedArray[i] = temp;
            i++;
        }

        return splittedArray;
    }
	
	public static String[] splitIgnoreStringBracketsCrochets(String s) {
        List<String> splitted = new ArrayList<>();
        StringBuilder tempString = new StringBuilder();
        boolean inString = false;
        for(char c : s.toCharArray()) {
            if(c == '"') {
                inString = !inString;
            }

            if(c != '[' && c != ']') {
                tempString.append(c);
            }else{
                if(!inString) {
                    splitted.add(tempString.toString());
                    tempString = new StringBuilder();
                }
            }
        }

        splitted.add(tempString.toString());

        String[] splittedArray = new String[splitted.size()];
        int i = 0;
        for(String temp : splitted) {
            splittedArray[i] = temp;
            i++;
        }

        return splittedArray;
    }
	
	public static boolean isAllUperCase(String s) {
		for(Character c : s.toCharArray()) {
			if(Character.isLowerCase(c)) {
                return false;
			}
		}
		
		return true;
	}
	
	public static String list(List<?> arg, char c) {
		String list = "";
		
		int i = 0;
		for(Object element : arg) {
            if(c == '\u0000') {
                list+=element.toString() + (i != 0 && i%3 == 0 ? "\n":" ");
            }else{
                list+=element.toString() + (i < arg.size() - 1 ? c:"") + (i != 0 && i%3 == 0 ? "\n":" ");
            }
			i++;
		}
		
		return list;
	}

    public static String list(String arg, char c) {
        List<String> args = new ArrayList<>();
        for(String s : split(arg, ' ')) {
            args.add(s);
        }

        return list(args, c);
    }
	
	public static List<String> toListFromString(String s) {
		if(!s.contains("\n")) return Collections.singletonList(s);
		List<String> list = new ArrayList<>();

        int i = 0;
		for(String part : s.split("\n")) {
            if(i != 0) {
                String colors = ChatColor.getLastColors(s);
                list.add(colors + part);
            }else{
                list.add(part);
            }
            i++;
		}
		
		return list;
	}

}
