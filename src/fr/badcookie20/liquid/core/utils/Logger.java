package fr.badcookie20.liquid.core.utils;

import org.bukkit.Bukkit;

import java.util.logging.Level;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the logger that MUST be used to broadcast info, warning and error messages involved with the backend/façade/frontend of the plugin
 * @author BadCookie20
 * @since InDev 1.0
 */
public class Logger {

    public static void logInfo(String msg) {
        Bukkit.getLogger().log(Level.INFO, msg);
    }

    public static void logWarning(String msg) {
        Bukkit.getLogger().log(Level.WARNING, msg);
    }

    public static void logError(String msg) {
        Bukkit.getLogger().log(Level.SEVERE, msg);
    }

}
