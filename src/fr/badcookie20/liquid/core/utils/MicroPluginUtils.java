package fr.badcookie20.liquid.core.utils;

import fr.badcookie20.liquid.core.LiquidPlugin;
import fr.badcookie20.liquid.core.commands.LiquidCommand;
import fr.badcookie20.liquid.core.editor.EditionPlayer;
import fr.badcookie20.liquid.core.events.LiquidEvent;
import fr.badcookie20.liquid.core.exceptions.LiquidEditionException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidIOException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.structs.StructType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is an SplitUtils class which contains methods involved with MicroPlugins
 * @author BadCookie20
 * @since InDev 1.0
 */
public class MicroPluginUtils {
	
	private static final String PREFIX = ChatColor.GREEN + ">";

	public static List<MicroPlugin> getAllMicroPlugins() throws LiquidException {
		List<MicroPlugin> plugins = new ArrayList<>();
		File dataFolder = LiquidPlugin.getInstance().getDataFolder();
        if(!dataFolder.exists()) {
            Logger.logWarning("The data folder does not exist");
            Logger.logInfo("Creating the data folder...");
            final boolean mkdir = dataFolder.mkdir();
            if(mkdir) {
                Logger.logInfo("Successfully created the data folder. You can now add microplugins!");
            }else{
                Logger.logError("Couldn't create the directory! Check the disk parameters!");
                throw new LiquidIOException("the plugin directory cannot be created!");
            }
            return plugins;
        }

		if(dataFolder.isFile()) throw new LiquidIOException("The data folder is a file!");
        if(dataFolder.listFiles() == null) return plugins;
        if(dataFolder.listFiles().length == 0) return plugins;

		for(File f : dataFolder.listFiles()) {
			if(f.isDirectory()) continue;
			
			if(f.getAbsolutePath().endsWith(".lmp")) {
                MicroPlugin p = new MicroPlugin(f);
				plugins.add(p);
			}
		}
	
		return plugins;
	}
	
	public static boolean hasClickedAnAction(ItemStack clickedAction) {
		return clickedAction != null && clickedAction.getType() == Material.SLIME_BLOCK && clickedAction.getItemMeta().getDisplayName().contains(">");
	}
	
	public static boolean hasClickedAnEvent(ItemStack clickedEvent) {
		return clickedEvent != null && clickedEvent.getType() == Material.BOOK && clickedEvent.getItemMeta().getDisplayName().contains(">");
	}

    public static boolean hasClickedAVarType(ItemStack clickedConstType) {
        return clickedConstType != null && clickedConstType.getType() == Material.REDSTONE_TORCH_ON && clickedConstType.getItemMeta().getDisplayName().contains(">");
    }

    public static boolean hasClickedAProperty(ItemStack clickedProperty) {
        return clickedProperty != null && clickedProperty.getType() == Material.BLAZE_ROD && clickedProperty.getItemMeta().getDisplayName().contains(">");
    }

    public static boolean hasClickedARequirement(ItemStack clickedRequirement) {
        return clickedRequirement != null && clickedRequirement.getType() == Material.COMPASS && clickedRequirement.getItemMeta().getDisplayName().contains(">");
    }

    public static MicroPlugin getMicroPluginByName(String name) {
        for(MicroPlugin microPlugin : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            if(microPlugin.getName().equalsIgnoreCase(name)) {
                return microPlugin;
            }
        }

        return null;
    }

    public static StructType getStruct(String name) {
        StructType struct = null;

        for(MicroPlugin mp : MicroPluginManager.getInstance().getAllMicroPlugins()) {
            if(struct != null) break;
            for(StructType structType : mp.getAllStructs()) {
                if(struct != null) break;
                if(structType.getName().equalsIgnoreCase(name)) {
                    struct = structType;
                }
            }
        }

        return struct;
    }

    public static String getLineToRemoveIfNeeded(MicroPlugin editing, String propertyName) {
        String propertyToRemove = propertyName.split(":")[0];
        String lineToRemove = null;

        for(String tempLine : editing.getAllLines()) {
            if(!tempLine.startsWith("-")) continue;
            String tempProperty = tempLine.split(":")[0];
            if(!tempProperty.equals(propertyToRemove)) continue;
            lineToRemove = tempLine;
        }

        return lineToRemove;
    }

    public static void addLine(MicroPlugin microPlugin, String line) throws LiquidEditionException {
        try {
            FileWriter writer = new FileWriter(microPlugin.getFile().getAbsolutePath(), true);
            writer.write(java.security.AccessController.doPrivileged(
                    new sun.security.action.GetPropertyAction("line.separator")));
            writer.write(line);
            writer.close();
        }catch(IOException e) {
            throw new LiquidEditionException("Error while accessing the micro-plugin " + microPlugin.getName() + " file!");
        }
    }

    public static void removeLines(MicroPlugin microPlugin, List<String> linesToRemove) throws LiquidEditionException {
        try {
            if (linesToRemove.isEmpty()) return;

            List<String> finalLines = new ArrayList<>();

            for (String lineToRemove : linesToRemove) {
                for (String line : microPlugin.getAllLines()) {
                    if (!line.equals(lineToRemove)) {
                        finalLines.add(line);
                    }
                }
            }

            microPlugin.getFile().delete();
            microPlugin.getFile().createNewFile();

            for (String line : finalLines) {
                addLine(microPlugin, line);
            }
        }catch(IOException e) {
            throw new LiquidEditionException("Error while trying to delete a line from the micro-plugin " + microPlugin.getFile() + " file!");
        }
    }
	
	public static Class<? extends Event> getEventInString(String fullLine) {
        fullLine = fullLine.split(" ")[0];

        for(LiquidEvent.EventList e : LiquidEvent.EventList.values()) {
            if(fullLine.equalsIgnoreCase(e.getName())) {
				return e.getEventClass();
			}
		}
		
		return null;
	}

    public static boolean areRequirementsEqual(List<LiquidRequirement> requirements1, List<LiquidRequirement> requirements2) {
        int index = 0;
        for(LiquidRequirement r1 : requirements1) {
            if(r1.getClassType().isAssignableFrom(requirements2.get(index).getClassType())
                    || requirements2.get(index).getClassType().isAssignableFrom(r1.getClassType())
                    || r1.getClassType().equals(requirements2.get(index).getClassType())) {
                index++;
                continue;
            }else{
                return false;
            }
        }

        return true;
    }

    public static boolean isLiquidCommandAlreadyRegistered(String cmdName, MicroPlugin microPlugin) {
        for(LiquidCommand cmd : microPlugin.getAllLiquidCommands()) {
            if(cmd.getName().equalsIgnoreCase(cmdName)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isVarAlreadyRegistered(String constName, MicroPlugin microPlugin) {
        for(LiquidParameter varDeclared : microPlugin.getAllVars()) {
            if(LangUtils.split(varDeclared.getLiquidObject().getPath(), '_')[2].equalsIgnoreCase(constName)) {
                return true;
            }
        }

        return false;
    }

	public static boolean isEditionInventory(Inventory inv) {
		return inv.getName().startsWith(">");
	}

    public static void deleteMicroPlugin(Player p, EditionPlayer editionPlayer) {
        if(!p.isOp()) {
            p.sendMessage(ChatColor.RED + "You cannot delete a micro-plugin if you're not op");
            return;
        }

        MicroPlugin microPlugin = editionPlayer.getEditing();

        boolean d = microPlugin.getFile().delete();

        if (!d) {
            p.sendMessage(ChatColor.RED + "Couldn't delete " + microPlugin.getName() + " micro-plugin!");
            return;
        }

        p.sendMessage(ChatColor.GREEN + "Successfully deleted the micro-plugin");
        editionPlayer.turnOffEditing();
    }

    public static boolean isDefLine(String line) {
        return line.startsWith(MicroPlugin.DEF_PREFIX);
    }

    public static String removeDefDeclaration(String fullLine) {
        StringBuilder sb = new StringBuilder(fullLine);
        sb.delete(0, 4);return sb.toString();
    }
}
