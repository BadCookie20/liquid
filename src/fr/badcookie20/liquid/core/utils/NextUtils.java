package fr.badcookie20.liquid.core.utils;

import fr.badcookie20.liquid.core.utils.annotations.NeedsOptimization;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.AsyncCatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class contains some methods that are useful for chat purposes
 * @author BadCookie20
 * @since Beta 1.3
 */
public class NextUtils {

    public static ItemStack CLOSED_INVENTORY = BukkitUtils.createItemStack(Material.BEDROCK, "ERR_CLOSED", null);

    private static Map<Player, String> sayings;
    private static List<Player> needsCancel;
    
    private static Map<Player, ItemStack> clickedItems;

    static {
        sayings = new HashMap<>();
        needsCancel = new ArrayList<>();
        
        clickedItems = new HashMap<>();
    }

    @NeedsOptimization
    public static String getNextMessageOf(Player p, boolean cancelIt) {
        if(!assertAsynCatcher()) return null;

        if(cancelIt && !needsCancel.contains(p)) needsCancel.add(p);

        String previousMessage = getLastMessageOf(p);

        if(previousMessage == null) {
            while(getLastMessageOf(p) == null) {

            }
        }else{
            while(getLastMessageOf(p).equals(previousMessage)) {

            }
        }

        return BukkitUtils.placeColor(getLastMessageOf(p));
    }

    public static String getLastMessageOf(Player p) {
        return sayings.get(p);
    }

    public synchronized static void updateChatOf(Player p, String message) {
        if(sayings.containsKey(p)) {
            sayings.replace(p, message);
        }else{
            sayings.put(p, message);
        }
    }

    public synchronized static boolean needsCancel(Player p) {
        return needsCancel.contains(p);
    }

    public synchronized static void updateCancel(Player p) {
        needsCancel.remove(p);
    }

    @NeedsOptimization
    public static ItemStack getNextClickedItemOf(Player p) {
        if (!assertAsynCatcher()) return null;

        ItemStack previousClick = getLastClickedItemOf(p);
        if (previousClick == CLOSED_INVENTORY) {
            previousClick = null;
            updateClickedItemOf(p, null);
        }

        if (previousClick == null) {
            while (getLastClickedItemOf(p) == null) {
            }
        } else {
            while (getLastClickedItemOf(p).equals(previousClick)) {
            }
        }

        if (previousClick == CLOSED_INVENTORY) return null;
        return getLastClickedItemOf(p);
    }

    public static ItemStack getLastClickedItemOf(Player p) {
        return clickedItems.get(p);
    }

    public synchronized static void updateClickedItemOf(Player p, ItemStack item) {
        if(clickedItems.containsKey(p)) {
        	clickedItems.replace(p, item);
        }else{
        	clickedItems.put(p, item);
        }
    }

    private static boolean assertAsynCatcher() {
        return !AsyncCatcher.enabled;
    }
    
}
