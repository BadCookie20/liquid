package fr.badcookie20.liquid.core.utils;

import fr.badcookie20.liquid.core.actions.ActionInstance;
import fr.badcookie20.liquid.core.condition.ConditionInstance;
import fr.badcookie20.liquid.core.condition.ConditionType;
import fr.badcookie20.liquid.core.exceptions.LiquidConditionStatementException;
import fr.badcookie20.liquid.core.exceptions.LiquidException;
import fr.badcookie20.liquid.core.exceptions.LiquidParameterException;
import fr.badcookie20.liquid.core.exceptions.LiquidReflectionException;
import fr.badcookie20.liquid.core.microplugin.MicroPlugin;
import fr.badcookie20.liquid.core.microplugin.MicroPluginManager;
import fr.badcookie20.liquid.core.reflect.LiquidObject;
import fr.badcookie20.liquid.core.reflect.LiquidParameter;
import fr.badcookie20.liquid.core.reflect.LiquidRequirement;
import fr.badcookie20.liquid.core.reflect.enums.AbstractLiquidEnum;
import fr.badcookie20.liquid.core.reflect.types.LiquidType;
import fr.badcookie20.liquid.core.structs.StructType;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class contains useful methods for Parameters purpose
 * @author BadCookie20
 * @since InDev 1.1.1
 */
public class ParameterUtils {

	public static boolean isString(String paramString) {
		return paramString.startsWith("\"") && paramString.endsWith("\"");
	}

    public static boolean isEnumValue(String paramString) {
        if(!paramString.contains(".")) {
            return false;
        }
        String[] parts = LangUtils.split(paramString, '.');

        return LangUtils.isAllUperCase(parts[0]) && LangUtils.isAllUperCase(parts[1]);
    }
    
    public static boolean isConstant(String path) {
		return path.startsWith("const");
	}

    public static boolean isVar(String path) {
        return path.startsWith("var");
    }

    public static boolean isBoolean(String paramString) {
        return paramString.equals("true") || paramString.equals("false");
    }

	public static ActionInstance fillInAction(ActionInstance a, List<Object> baseObjects) throws LiquidException {
		if(!a.getRelatedAction().isStatic()) {
            a.replaceInstance(fillInParam(a.getInstance(), baseObjects));
        }
		a.replaceArguments(fillInParams(a.getArguments(), baseObjects));
        if(a.getCondition() != null) {
            a.replaceCondition(fillInConditionMap(a.getCondition(), baseObjects));
        }
		return a;
	}


	public static LiquidParameter fromStringToParam(String paramString) throws LiquidParameterException, LiquidReflectionException {
        boolean unereasable = false;
		LiquidParameter paramLiquid = new LiquidParameter(paramString);

		if(isBoolean(paramString)) {
			unereasable = true;
			paramLiquid.setContent(Boolean.parseBoolean(paramString));
		}else if(isString(paramString)) {
            unereasable = true;
            paramLiquid.setContent(new StringBuilder(paramString).deleteCharAt(0).deleteCharAt(paramString.length() - 2).toString());
        }else if(isGetter(paramString)) {
            String[] parts = LangUtils.splitIgnoreStringBracketsParentheses3parts(paramString);
            String property = (String) fromStringToParam(parts[1]).getContent();
            String name = new StringBuilder(parts[2]).delete(0, 2).toString();

            StructType struct = MicroPluginUtils.getStruct(name);
            if(struct == null) throw new LiquidParameterException("unknown structs " + name + " in statement " + paramString);
            LiquidRequirement structRequirement = struct.getTypeOfProperty(property);
            if(structRequirement == null) throw new LiquidParameterException("unknown property " + property + " in structs " + name);
            paramLiquid = new LiquidParameter(paramString, structRequirement);
        }else if(isEnumValue(paramString)) {
            unereasable = true;
            paramLiquid.setContent(AbstractLiquidEnum.EnumList.getEnumValue(paramString));
        }else if(NumberUtils.isNumber(paramString)) {
            unereasable = true;
            paramLiquid.setContent(Integer.parseInt(paramString));
        }else{
            MicroPluginManager instance = MicroPluginManager.getInstance();
            List<MicroPlugin> microPlugins = instance.getAllMicroPlugins();
            for(MicroPlugin mp : microPlugins) {
                if((mp.getAllVars() == null || mp.getAllVars().isEmpty()) && (mp.getAllStructs() == null || mp.getAllStructs().isEmpty())) {
                    continue;
                }

                for(LiquidParameter var : mp.getAllVars()) {
                    String varFullName = var.getLiquidObject().getPath();
                    String varDecName = LangUtils.split(varFullName, '_')[2];
                    boolean isConstant = LangUtils.split(varFullName, '_')[0].equals("const");
                    if(varDecName.equalsIgnoreCase(paramString)) {
                        if(isConstant) unereasable = true;
                        paramLiquid.setContent(var.getContent());
                    }
                }

                for(StructType struct : mp.getAllStructs()) {
                    if(struct.getName().equalsIgnoreCase(paramString)) {
                        unereasable = true;
                        paramLiquid.setContent(struct);
					}
                }
            }
        }
	
	    paramLiquid.setUnereasable(unereasable);
	    return paramLiquid;
	}

	public static List<LiquidParameter> fillInParams(List<LiquidParameter> params, List<Object> baseObjects) throws LiquidParameterException, LiquidReflectionException {
        List<LiquidParameter> finalParams = new ArrayList<>();
        for(LiquidParameter param : params) {
	        finalParams.add(ParameterUtils.fillInParam(param, baseObjects));
	    }

	    return finalParams;
	}

	public static LiquidParameter fillInParam(LiquidParameter param, List<Object> baseObjects) throws LiquidParameterException, LiquidReflectionException {
        if(param.isUnereasable() && param.getLiquidObject().getPath().contains("%")) {
			String s = param.getLiquidObject().getPath();
            StringBuilder sb = new StringBuilder(s);
            sb.deleteCharAt(0);
            sb.deleteCharAt(sb.length() - 1);
            return fillStringValue(sb.toString(), baseObjects);
        }

        if(param.isUnereasable()) return param;

        LiquidObject liquidObject = param.getLiquidObject();

        if(isGetter(liquidObject.getPath())) {
            String[] parts = LangUtils.splitIgnoreStringBracketsParentheses(liquidObject.getPath());
            String property = (String) fromStringToParam(parts[1]).getContent();
            String name = new StringBuilder(parts[2]).delete(0, 2).toString();
            StructType struct = MicroPluginUtils.getStruct(name);
            param.setContent(struct.getProperty((String) fillStringValue(property, baseObjects).getContent()));
            return param;
        }

        if(isEnumValue(liquidObject.getPath())) {
            param.setContent(AbstractLiquidEnum.EnumList.getEnumValue(liquidObject.getPath()));
        }
        
        if(isConstant(liquidObject.getPath())) {
            return param;
        }

        if(isVar(liquidObject.getPath())) {
            System.out.println("isVar: " + liquidObject.getPath());
            // todo (check if is a var: then take the last value of the var)
            System.out.println("content = " + param.getContent());
            for(MicroPlugin microPlugin : MicroPluginManager.getInstance().getAllMicroPlugins()) {
                for(LiquidParameter var : microPlugin.getAllVars()) {
                    if (var.getLiquidObject().getPath().equals(liquidObject.getPath())) {
                        System.out.println("found var! New content: " + var.getContent());
                        return var;
                    }
                }
            }
            throw new LiquidParameterException("the value of the var " + LangUtils.split(liquidObject.getPath(), '_')[2] + " is empty!");
        }
        
        if(isBoolean(liquidObject.getPath())) {
        	return param;
        }

	    param = matchParamWithObjects(param, baseObjects);
	
	    if(param.getContent() == null) {
	        throw new LiquidParameterException("none of the specified types fitted the parameter while trying to fill it!");
	    }
	
	    return param;
	}
	
	private static LiquidParameter matchParamWithObjects(LiquidParameter param, List<Object> baseObjects) throws LiquidParameterException {
		LiquidObject liquidObject = param.getLiquidObject();
	    LiquidType superType = liquidObject.getSuperType();

        // superType may be null

        LiquidRequirement preAssignable = null;
        if(isGetter(param.getLiquidObject().getPath())) {
            preAssignable = param.getLiquidObject().getRequirement();
        }

		if(!liquidObject.hasSubType()) {
            for(Object o : baseObjects) {
				if(superType == null && preAssignable == null) {
                    if (NumberUtils.isNumber(liquidObject.getPath())) {
                        param.setContent(new Integer(liquidObject.getPath()));
                    }
                }else if(preAssignable != null) {
                    if(!preAssignable.getClassType().isAssignableFrom(o.getClass())) {
                        continue;
                    }
                }else if(!superType.getClassType().isAssignableFrom(o.getClass())) {
                        continue;
	            }
                param.setContent(o);
	        }
	    }else{
	        for(Object o : baseObjects) {
                if(!superType.getClassType().isAssignableFrom(o.getClass())) {
                    continue;
	            }
	            List<LiquidRequirement> assignableRequirements = superType.getAssignableRequirements();
	            for(LiquidRequirement assignable : assignableRequirements) {
                    if(assignable.equals(liquidObject.getRequirement())) {
                        param.setContent(superType.get(liquidObject.getPath(), o));
                        break;
	                }
	            }
	        }
	    }
		
		return param;
	}
	
	private static LiquidParameter fillStringValue(String paramString, List<Object> possibleObjects) throws LiquidParameterException {
        String[] parts = LangUtils.split(paramString, '%');
		String finalString = "";
		
		for(String s : parts) {
            if(s == null || s.isEmpty()) continue;
            if(!s.contains(" ") && s.contains(".")) {
                LiquidParameter tempParam = new LiquidParameter(s);
				tempParam = matchParamWithObjects(tempParam, possibleObjects);
                finalString+=tempParam.getContent();
            }else{
                finalString+=s;
			}
		}

        LiquidParameter param =  new LiquidParameter("\"" + paramString + "\"");
        param.setContent(finalString);
        param.setUnereasable(true);
        return param;
    }

    public static ConditionInstance fillInConditionMap(ConditionInstance map, List<Object> baseObjects) throws LiquidParameterException, LiquidReflectionException {
        map.setParam1(fillInParam(map.getParam1(), baseObjects));
	    map.setParam2(fillInParam(map.getParam2(), baseObjects));
	    return map;
	}

	@Deprecated
	public static LiquidParameter customFillInParameter(LiquidParameter param, Object baseObject) throws LiquidParameterException {
		if(param.getContent() != null) {
	        return param;
	    }
		LiquidType paramType = param.getLiquidObject().getSuperType();
		
		if(param.getLiquidObject().getRequirement() == LiquidRequirement.getRequirementByClass(baseObject.getClass())) {
			param.setContent(baseObject);
			return param;
		}
		
		String[] splitted = LangUtils.split(param.getLiquidObject().getPath(), '.');
	    param.setContent(paramType.getSubtype(splitted[1], baseObject));
	    return param;
	}

	public static String getConditionIndicator(String condition) {
		for(String ind : ConditionType.getAllIndicators()) {
			if(condition.contains(ind)) {
				return ind;
			}
		}
		
		return null;
	}
	
	public static String[] toConditionParts(String condition) throws LiquidConditionStatementException {
		if(getConditionIndicator(condition) == null) {
			throw new LiquidConditionStatementException("the condition statement " + condition + " does not contain any condition checker!");
		}
		
		String indicator = getConditionIndicator(condition);
		
		StringBuilder sb = new StringBuilder(condition);
		int index = sb.indexOf(indicator);
		String[] parts = new String[2];
		parts[0] = sb.substring(0, index);
		parts[1] = sb.substring(index + 2, sb.length());
		
		return parts;
	}

    public static boolean isGetter(String path) {
        return path.startsWith("get");
    }
}
