package fr.badcookie20.liquid.core.utils.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This annotation is used to define a new method that is preferred to another one but that may contain bugs or
 * that may be corrupted (antagonist of @Deprecated)
 * @author BadCookie20
 * @since Beta 1.0
 * 
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Preferred {

}
