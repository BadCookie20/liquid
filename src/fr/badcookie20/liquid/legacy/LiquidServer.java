package fr.badcookie20.liquid.legacy;

import fr.badcookie20.liquid.legacy.event.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
public class LiquidServer {

	private static List<EventListener> listeners = new ArrayList<>();
	private static Map<EventListener, Method> eventsMethods = new HashMap<>();

    /**
     * Allows the server to call a custom event. It calls all the registered and ready listener methods that have the <code>@EventMethod</code> annotation
     * @param e the event to call. It must be custom
     * @throws EventException if the event is not custom (i.e. natural) of if method called does not fit the <code>@EventMethod</code> pattern
     */
	public static void callCustomEvent(Event e) throws EventException {
		if(!e.isCustom()) {
			throw new EventException("the specified event is not custom !");
		}
		
		callEvent(e);
	}

    /**
     * Allows the server to call a natural event. It calls all the listeners and ready methods that have the <code>@EventMethod</code> annotation
     * @param e the event to call. It must be natural
     * @throws EventException if the event is not natural (i.e. custom) or if method called does not fit the <code>@EventMethod</code> pattern
     */
	public static void callNaturalEvent(Event e) throws EventException {
		if(!e.isNatural()) {
			throw new EventException("the specified event is not natural !");
		}
		
		callEvent(e);
	}

    /**
     * Allows the server to call an event. It calls all the listeners and ready methods that have the <code>@EventMethod</code> annotation
     * @param e the event to call. It can either be natural or custom
     * @throws EventException if the method called does not fit the <code>@EventMethod</code> pattern
     */
	private static void callEvent(Event e) throws EventException {
		for(Method m : eventsMethods.values()) {
			EventListener correspondingListener = null;
			for(EventListener l : eventsMethods.keySet()) {
				if(eventsMethods.get(l).equals(m)) {
					correspondingListener = l;
				}
			}
			Parameter p = m.getParameters()[0];
			if(p.getType().equals(e.getClass())) {
				if(!m.isAccessible()) {
					m.setAccessible(true);
				}
				try {
					m.invoke(correspondingListener, e);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
					e1.printStackTrace();
					throw new EventException("an error occured while trying to invoke the event !");
				}	
			}
		}
	}
	
	public static void registerListener(EventListener l) throws ListenerException {
		if(listeners.contains(l)) {
			throw new ListenerException("the specified EventListener is already registered !");
		}
		listeners.add(l);
	}
	
	public static void ready() throws ListenerException {
		for(EventListener l : listeners) {
			for(Method m : l.getClass().getMethods()) {
				if(m.isAnnotationPresent(EventMethod.class)) {
					if(!Event.isCorrectEventMethod(m)) {
						throw new ListenerException("the method " + m.getName() + " in listener " + l.getClass().getSimpleName() + " does not fit the standard listener method !");
					}else{
						eventsMethods.put(l, m);
					}
				}
			}
		}
	}
	
}
