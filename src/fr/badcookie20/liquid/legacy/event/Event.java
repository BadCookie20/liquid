package fr.badcookie20.liquid.legacy.event;

import java.lang.reflect.Method;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This class is the abstract subclass of all the <code>LiquidEvents</code>. All the events must extend this class.
 */
public abstract class Event {
	
	private Reason r;

    /**
     * Constructor of the event. When it is initialized, the <code>LiquidServer</code> does not call it. You can call it by doing <code>LiquidServer.callCustomEvent()</code> or
     * <code>LiquidServer.callNaturalEvent()</code>
     * @param r the reason (natural or custom) of this event
     */
	public Event(Reason r) {
		this.r = r;
	}

    /**
     * Allows to know if this event is natural or not
     * @return <code>true</code> if this event is natural or <code>false</code> if not
     */
	public boolean isNatural() {
		return this.r == Reason.NATURAL;
	}

    /**
     * Allows to know if this event is custom or not
     * @return <code>true</code> if this event is custom or <code>false</code> if not
     */
	public boolean isCustom() {
		return this.r == Reason.CUSTOM;
	}

    /**
     * Allow to know if the specified method is a correct listener method (i.e. if the only parameter is an <code>Event</code> Object).
     * </bolc>Note: the <code>@EventMethod</code> annotation must be present
     * @param m the method to check
     * @return <code>true</code> if the method fits the pattern, and <code>false</code> if not
     */
	public static boolean isCorrectEventMethod(Method m) {
		Class<?>[] parameters = m.getParameterTypes();
		if(parameters.length != 1) {
			return false;
		}

		return Event.class.isAssignableFrom(parameters[0]);

	}
	
}
