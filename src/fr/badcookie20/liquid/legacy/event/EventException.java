package fr.badcookie20.liquid.legacy.event;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * This exception is called when an exception involved with events is thrown
 * @author BadCookie20
 */
public class EventException extends Exception {

	private static final long serialVersionUID = 1740614121731612308L;

	/**
	 * Constructor of the <code>Exception</code>
	 * @param reason the reason why this exception is called
     */
	public EventException(String reason) {
		super(reason);
	}

}
