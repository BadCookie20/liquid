package fr.badcookie20.liquid.legacy.event;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * Event call reason. It may be <code>natural</code> or <code>custom</code>
 */
public enum Reason {

	NATURAL,
	CUSTOM;
	
}
