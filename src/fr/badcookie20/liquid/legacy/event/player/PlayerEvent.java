package fr.badcookie20.liquid.legacy.event.player;

import fr.badcookie20.liquid.legacy.event.Event;
import fr.badcookie20.liquid.legacy.event.Reason;
import fr.badcookie20.liquid.legacy.player.Player;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/**
 * Superclass of all the events involved with players
 */
public abstract class PlayerEvent extends Event {

	private Player p;

    /**
     * Constructor of the player event. When it is initialized, the <code>LiquidServer</code> does not call it. You can call it by doing <code>LiquidServer.callCustomEvent()</code> or
     * <code>LiquidServer.callNaturalEvent()/code>. This event must be involved with a player
     * @param r the reason (natural or custom) of this event
     * @param p the player involved with this event
     */
	public PlayerEvent(Reason r, Player p) {
		super(r);
		this.p = p;
	}

    /**
     * Returns the player involved with this event
     * @return the player involved with this event
     */
	public Player getPlayer() {
		return this.p;
	}

}
