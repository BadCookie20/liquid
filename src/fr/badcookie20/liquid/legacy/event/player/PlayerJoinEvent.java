package fr.badcookie20.liquid.legacy.event.player;

import fr.badcookie20.liquid.legacy.event.Reason;
import fr.badcookie20.liquid.legacy.player.Player;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
public class PlayerJoinEvent extends PlayerEvent {

	/**
	 * Constructor of the PlayerJoinEvent. The reason is <code>natural</code>
	 * @param p the player which joins the game
     */
	public PlayerJoinEvent(Player p) {
		super(Reason.NATURAL, p);
	}

}
